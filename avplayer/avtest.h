/*  This file is part of the KDE project
    Copyright (C) 2006 Allan Sandfeld Jensen <kde@carewolf.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 2
    as published by the Free Software Foundation.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
    02110-1301, USA.

*/

#ifndef AVPLAYER_H
#define AVPLAYER_H

#include <QtGui/QWidget>
#include <QtCore/QThread>
#include <QtCore/QSemaphore>


namespace AvKode {
    class FFMPEGDecoder;
    class VideoSink;
    class ALSASink;
    class ByteStream;
    class Synchronizer;
}

class PlayThread;
class MediaControl;

class AvPlayer : public QWidget
{
    Q_OBJECT
public:
    AvPlayer( QWidget* parent = 0 );
    ~AvPlayer();
    void setUrl( QString );
public slots:
    void play();
    void pause();
    void stop();
private:
    AvKode::ByteStream *file;
    PlayThread *thread;
    AvKode::VideoSink *vSink;
    MediaControl *control;
};

class PlayThread : public QThread
{
    Q_OBJECT
public:
    PlayThread(AvKode::ByteStream *file, AvKode::VideoSink *vSink);
    void play();
    void stop();
    void pause();
public slots:
    void seek(int i);
signals:
    void position(int i);
protected:
    void open();
    void close();
    void run();
private:
//     const char* url;
    AvKode::ByteStream *file;
    AvKode::ALSASink *aSink;
    AvKode::VideoSink *vSink;
    AvKode::FFMPEGDecoder *decoder;
    AvKode::Synchronizer *synchronizer;
    QSemaphore call_play;
    volatile bool halt;
    volatile bool suspend;
    volatile int seek_pos;
};

#endif
