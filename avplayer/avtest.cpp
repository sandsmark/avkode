/*
    Copyright (C) 2006 Allan Sandfeld Jensen <kde@carewolf.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 2
    as published by the Free Software Foundation.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
    02110-1301, USA.

*/

#include "avtest.h"

#include <kcmdlineargs.h>
#include <kapplication.h>
#include <kaboutdata.h>
#include <kiconloader.h>
#include <kurl.h>
#include <kio/job.h>

#include <QtGui/QVBoxLayout>
#include <QtGui/QHBoxLayout>
#include <QtCore/QString>
#include <QtCore/QThread>
#include <QtGui/QToolButton>
#include <QtGui/QSlider>
#include <QtCore/QDebug>
#include <iostream>

#include "avframe.h"
#include "ffmpeg_decoder.h"
#include "alsa_sink.h"
#include "file.h"
#include "kio.h"
#include "bytestream.h"
#include "qt4_sink.h"
#include "xv_sink.h"
#include "synchronizer.h"


PlayThread::PlayThread(AvKode::ByteStream *file, AvKode::VideoSink *vSink)
               : file(file), vSink(vSink), halt (false), seek_pos(-1), suspend(true) {};

void PlayThread::play() {
    suspend = false;
    halt = false;
    call_play.release();
    if (!isRunning()) start();
}
void PlayThread::stop() {
    halt = true;
    if (suspend)
        call_play.release();
    if (isRunning()) {
        file->release(); // make sure no calls are blocking on file
        wait();
    }
    std::cout << "PlayThread stopped" << std::endl;
}
void PlayThread::pause() {
    if (suspend)
        play();
    else
        suspend = true;
}
void PlayThread::seek(int i) {
    seek_pos = i;
}

void PlayThread::open() {
    decoder = new AvKode::FFMPEGDecoder();
    aSink = new AvKode::ALSASink();
    synchronizer = new AvKode::Synchronizer();
    if (file->error()) file->reset();
    file->waitFor(8192);
    bool s = decoder->open(file);
    if (!s) {
        std::cout << "Could not open file" << std::endl;
        exit(1);
    }
    s = aSink->open();
    if (!s) {
        std::cout << "Could not open sink" << std::endl;
        exit(1);
    }

//     vSink->open();

    synchronizer->setAudioSink(aSink);
    synchronizer->setVideoSink(vSink);
}

void PlayThread::close() {
    delete synchronizer;
    delete decoder;
    aSink->close();
    delete aSink;
}

void PlayThread::run() {
    open();
    call_play.acquire();
    if (halt) {
        close();
        return;
    }

    AvKode::AudioFrame aFrame;
    AvKode::VideoFrame vFrame;
    AvKode::FrameType type;
    int pos = 0;
    long len = decoder->length();
    emit position(0);
    std::cout << "Length: " << (len/60000) <<  ":" << ((len /1000) % 60) << "\n";
    while(true) {
        if (suspend)
            call_play.acquire();
        if (halt) break;
        if (seek_pos > 0) {
            assert(seek_pos <= 1024);
            long pos = len * (seek_pos / 1024.0);
            if( decoder->seek(pos))
                emit position(seek_pos);
            else
                emit position(pos);
            seek_pos = -1;
        }
        if (decoder->frameType() == AvKode::NoFrameType) {
            if (!decoder->decodeFrame()) {
                if (decoder->eof() || decoder->error())
                    break;
                else
                    continue;
            }
        }
        type = decoder->frameType();
        switch (type) {
            case AvKode::AudioFrameType: {
                decoder->readAudioFrame(&aFrame);
                synchronizer->writeAvFrame(&aFrame);
                break;
            }
            case AvKode::VideoFrameType: {
                decoder->readVideoFrame(&vFrame);
                synchronizer->writeAvFrame(&vFrame);
                break;
            }
            default:
                break;
        }
        long dpos = decoder->position();
        if (dpos != -1) {
            long npos = (dpos * 1024.0) / (float)len;
            if ((npos > 0) && (npos < 1024) && pos != npos) {
                emit position(npos);
                pos = npos;
            }
            if (seek_pos == pos) seek_pos = -1;
        }
        if (decoder->eof() || decoder->error()) break;
    }
    close();
}

// MediaControl is copied from phonon/ui/mediacontrol.h
class MediaControl : public QWidget
{
public:
    MediaControl(QWidget *parent)
        : QWidget(parent)
        , layout( 0 )
        , playButton( 0 )
        , pauseButton( 0 )
        , stopButton( 0 )
/*        , loopButton( 0 )
        , seekSlider( 0 )
        , volumeSlider( 0 )*/
{
    playButton = new QToolButton( this );
    playButton->setIcon( SmallIcon( "media-playback-start" ) );
    playButton->setText( "&Play" );
    playButton->setFixedSize( 20, 20 );
    playButton->setAutoRaise( true );

    pauseButton = new QToolButton( this );
    pauseButton->setIcon( SmallIcon( "media-playback-pause" ) );
    pauseButton->setText( "&Pause" );
    pauseButton->setFixedSize( 20, 20 );
    pauseButton->setAutoRaise( true );

    stopButton = new QToolButton( this );
    stopButton->setIcon( SmallIcon( "media-playback-stop" ) );
    stopButton->setText( "&Stop" );
    stopButton->setFixedSize( 20, 20 );
    stopButton->setAutoRaise( true );

    seekSlider = new QSlider( Qt::Horizontal, this );
    seekSlider->setFixedHeight( 20 );
    seekSlider->setRange(0, 1024);
    seekSlider->setEnabled(false);

    layout = new QHBoxLayout( this );
    layout->setMargin( 0 );
    layout->setSpacing( 2 );
    layout->addWidget( playButton );
    layout->addWidget( pauseButton );
    layout->addWidget( stopButton );
    layout->addWidget( seekSlider );
}

void connectPlayer(AvPlayer *player, PlayThread *thread) {
    connect( playButton, SIGNAL( clicked() ), player, SLOT( play() ) );
    connect( pauseButton, SIGNAL( clicked() ), player, SLOT( pause() ) );
    connect( stopButton, SIGNAL( clicked() ), player, SLOT( stop() ) );
    connect( seekSlider, SIGNAL( valueChanged(int) ), thread, SLOT( seek(int) ) );
    connect( thread, SIGNAL( position(int) ), seekSlider, SLOT( setValue(int) ) );
}

private:
    QHBoxLayout* layout;
    QToolButton* playButton;
    QToolButton* pauseButton;
    QToolButton* stopButton;
//         QToolButton* loopButton;
    QSlider*  seekSlider;
//         VolumeSlider* volumeSlider;
};

AvPlayer::AvPlayer( QWidget* parent )
        : QWidget( parent ), vSink(0)
{
    QLayout *layout = new QVBoxLayout;
    setLayout(layout);
    setMinimumSize(600, 480);
    AvKode::XVideoSink *videosink = new AvKode::XVideoSink(this);
//     AvKode::Qt4VideoSink *videosink = new AvKode::Qt4VideoSink(this);
    vSink = videosink;
    layout->addWidget(videosink);
    vSink->open();
    control = new MediaControl(this);
    control->setFixedHeight( 30 );
    layout->addWidget(control);
}

AvPlayer::~AvPlayer() {
    thread->stop();
    delete thread;
    delete file;
}

void AvPlayer::setUrl( QString path)
{
    qDebug() << "opening " << path;
    KUrl url(path);
    //file = AvKode::KIO::open(url);
    file = AvKode::File::open(path.toLatin1());

    thread = new PlayThread(file, vSink);
    thread->start();
    control->connectPlayer(this, thread);
}

void AvPlayer::play() {
    thread->play();
}

void AvPlayer::pause() {
    thread->pause();

}

void AvPlayer::stop() {
    thread->stop();
}

int main( int argc, char ** argv )
{
    KAboutData about( "avtest", "avtest", ki18n("AVKode Test Player"),
                      "0.3", ki18n("Media Player"),
                      KAboutData::License_GPL);
    about.addAuthor( ki18n("Allan Sandfeld Jensen"), ki18n("author"), "kde@carewolf.com" );
    about.addAuthor( ki18n("Martin Sandsmark"), ki18n("developer"), "martin.sandsmark@kde.org" );
    KCmdLineOptions options;
    options.add("+[file]", ki18n("URL to open."));
    KCmdLineArgs::addCmdLineOptions(options);
    KCmdLineArgs::init( argc, argv, &about );
    KApplication app;
    AvPlayer foo;
    KCmdLineArgs *args = KCmdLineArgs::parsedArgs( );
    if (args->count() == 1)
        foo.setUrl(args->arg(0));
    else
        foo.setUrl(getenv( "AVKODE_TESTURL"));
    app.processEvents();
    foo.show();
    return app.exec();
}

#include "avtest.moc"
