/*  AvKode: Qt4 Video-sink

    Copyright (C) 2006 Allan Sandfeld Jensen <sandfeld@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Steet, Fifth Floor,
    Boston, MA 02110-1301, USA.
*/

#include "xv_sink.h"
#include "avframe.h"
#include "avtime.h"

#include <QtGui/QPainter>
#include <QtGui/QX11Info>
#include <QtCore/QMutex>
#include <QtCore/QSize>
#include <QtGui/QResizeEvent>
#include <QtGui/QApplication>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xatom.h>

#include <X11/extensions/Xv.h>
#include <X11/extensions/Xvlib.h>
// #include <X11/extensions/XShm.h>

#include <assert.h>
#include <iostream>

namespace AvKode {

#define XV_RGB32 (0x3)
#define XV_YUY2 (0x32595559)
#define XV_I420 (0x30323449)
#define XV_YV12 (0x32315659)

template<int i>
struct LatencyCalculator {
    LatencyCalculator() : number(0), index(0), total(0){};
    void addLatency(int lat) {
        if (number < i) {
            number += 1;
            total += lat;
        } else {
            total -= latency[index];
            total += lat;
        }
        latency[index] = lat;
        index += 1;
        if (index == i) index = 0;
    }
    int average() {
        if (number > 0)
            return total /number;
        else
            return 0;
    }
    int latency[i];
    int number;
    int index;
    int total;
};

struct XVideoSink::private_data {
    private_data() : display(0), window(0), xv_port(-1), xv_image(0), newframe(false) {}

    Display *display;
    Window window;
    GC gc;

    int xv_port;
    XvImage *xv_image;

    QMutex mutex; // mutex for protecting vframe
    VideoFrame vframe;
    bool newframe;

    LatencyCalculator<8> latency;

    uint8_t color_formats;
    VideoConfiguration config;
};

XVideoSink::XVideoSink(QWidget *parent) : QWidget(parent)
{
    d = new private_data;
    setAttribute(Qt::WA_NoSystemBackground, true);
    setAttribute(Qt::WA_PaintOnScreen, true);
    setAttribute(Qt::WA_OpaquePaintEvent, true);
    
    QPalette pal = palette();
    pal.setColor(backgroundRole(), Qt::black);
    setPalette(pal);
}

XVideoSink::~XVideoSink()
{
    if (d->xv_port != -1) close();
    delete d;
}

bool XVideoSink::open()
{
    d->display = x11Info().display();
    d->window = winId();

    // read QWidget size
    d->config.width = size().width();
    d->config.height = size().height();

    d->gc=XCreateGC(d->display,d->window, 0, NULL);

    if (!checkXv()) {
        std::cout << "Xv failed\n";
        return false;
    }

    sizePolicy().setHeightForWidth(true);

    connect(this, SIGNAL(doUpdate()), this, SLOT(repaint()));

    return true;
}

bool XVideoSink::checkXv()
{
    std::cout << "XVideoSink::checkXv()" << std::endl;
    int ret;
/*  I don't use Shm yet
    ret = XShmQueryExtension(xWindow->display));
    if (ret != Success)
        return false;
*/
    unsigned int p_version=0, p_release=0, p_request_base=0;
    unsigned int p_event_base=0, p_error_base=0;

    ret = XvQueryExtension(d->display,
                           &p_version, &p_release, &p_request_base,
                           &p_event_base, &p_error_base);
    if (ret != Success) {
        std::cerr << "XVideoSink: QueryExtension failed" << std::endl;
        return false;
    }

    unsigned int p_num_adaptors=0;
    XvAdaptorInfo *ai;
    ret = XvQueryAdaptors(d->display, d->window,
                          &p_num_adaptors, &ai);

    if (ret != Success) {
        std::cerr << "XVideoSink: QueryAdaptors failed" << std::endl;
        return false;
    }
    if (p_num_adaptors == 0)
        return false;

    int overlay = -1;
    int blitter = -1;
    int other = -1;
    // Search for a suitable adaptor
    for (unsigned int i = 0; i < p_num_adaptors && d->xv_port == -1; i++)
    {
        if ((ai[i].type & XvInputMask) && (ai[i].type & XvImageMask))
        {
            std::cout << "XVideoSink: Checking base-port " << ai[i].base_id << std::endl;

            QString name = QString::fromAscii(ai[i].name);
            if (name.endsWith("Overlay")) overlay = i;
            else
            if (name.endsWith("Blitter")) blitter = i;
            else
                other = i;
        }
    }

    int adaptor = -1;

    if (blitter != -1) {
        std::cout << "XVideoSink: Found blitter adaptor" << std::endl;
        adaptor = blitter;
    }
    else
    if (overlay != -1) {
        std::cout << "XVideoSink: Only found overlay adaptor" << std::endl;
        adaptor = overlay;
    }
    else
    if (other != -1) {
        std::cout << "XVideoSink: Found unrecognized adaptor" << std::endl;
        adaptor = other;
    }

    if (adaptor != -1) {
        d->color_formats = 0;
        int nr_formats;
        XvImageFormatValues *ifvs = XvListImageFormats(d->display, ai[adaptor].base_id, &nr_formats);
        for(int j=0; j<nr_formats; j++) {
            switch(ifvs[j].id) {
            case XV_RGB32:
                d->color_formats |= RGB32;
                break;
            case XV_I420:
                d->color_formats |= YV12;
                break;
            case XV_YUY2:
                d->color_formats |= YUY2;
                break;
            default:
                break;
            }
        }
        XFree(ifvs);

        for (XvPortID xv_p = ai[adaptor].base_id;
            xv_p < ai[adaptor].base_id + ai[adaptor].num_ports; ++xv_p)
        {
//             if (XvGrabPort(d->display, xv_p, ?time?) == Success)
            d->xv_port = xv_p;
            break;
        }
    }

    if (p_num_adaptors > 0)
        XvFreeAdaptorInfo(ai);

    if (d->xv_port == -1) {
        std::cerr << "XVideoSink: No Xv port found" << std::endl;
        return false;
    } else
        std::cout << "XVideoSink: Using Xv port: " << d->xv_port << std::endl;

    return true;
}

void XVideoSink::close()
{
    if (d->xv_image) {
        XFree(d->xv_image);
        d->xv_image = 0;
    }
    XFreeGC(d->display, d->gc);
    d->window  = 0;

    // ### release Xv port if grapped
    d->xv_port = -1;
}

static int avformat2xv(int i)
{
    switch (i) {
        case RGB32:
            return XV_RGB32;
        case YUY2:
            return XV_YUY2;
        case YV12:
            return XV_I420;
        default:
            return 0;
    }
}

bool XVideoSink::writeVideoFrame(VideoFrame *vFrame)
{

    if (!d->display || !d->window) return false;

    d->mutex.lock();

    swapVideoFrames(&d->vframe, vFrame);
    d->newframe = true;

    d->mutex.unlock();

    emit doUpdate();

    return true;
}

void XVideoSink::paintEvent(QPaintEvent*)
{
    bool newframe = d->newframe; // copy for thread-safety

    if (newframe) {
        if (d->xv_image) {
            XFree(d->xv_image);
            d->xv_image = 0;
        }

        d->mutex.lock(); // lock while we are using shared variable d->vframe

        d->xv_image = XvCreateImage(d->display, d->xv_port,
                                    avformat2xv(d->vframe.color_format), (char*)d->vframe.data,
                                    d->vframe.width, d->vframe.height);
        if (!d->xv_image) {
            std::cout << "XVideoSink: CreateImage failed" << std::endl;
            d->newframe = false;
            d->mutex.unlock();
            return;
        }
    }

    QPainter p;
    p.begin(this);

    if (d->xv_image)  {
        // Scale up to aspect ratio
        float wratio = d->vframe.width/(float)d->vframe.aspect_ratio.width;
        float hratio = d->vframe.height/(float)d->vframe.aspect_ratio.height;
        float ratio = qMax(wratio, hratio);
        QSize frame_size = QSize((int)(d->vframe.aspect_ratio.width  * ratio),
                             (int)(d->vframe.aspect_ratio.height * ratio));
        // resize to widget
        frame_size.scale(size(), Qt::KeepAspectRatio);

    //     QRect paint_rect(0, 0, frame_size.width(), frame_size.height());
    //     paint_rect = paint_rect.intersect(update_rect);

    //     std::cout << "Image size: " << image_size << std::endl;
    //     std::cout << "Frame size: " << frame_size << std::endl;
        int x = width() - frame_size.width();
        x /= 2;
        if (x < 0) x = 0;
        int y = height() - frame_size.height();
        y /= 2;
        if (y < 0) y = 0;
        XvPutImage(d->display, d->xv_port, d->window, d->gc, d->xv_image,
                   0, 0, d->vframe.width, d->vframe.height,
                   x, y, frame_size.width(), frame_size.height());

        // clear unpainted area
        QRegion frame(x, y, frame_size.width(), frame_size.height());
        QRegion rbackground = QRegion(rect()) - frame;
        foreach(QRect rect, rbackground.rects())
            p.fillRect(rect, p.background());
        p.end();
        QApplication::syncX();
//         XSync(d->display, False);
    }
    else {
        p.fillRect(QRect(QPoint(0,0), size()), p.background());
        p.end();
    }
    if (newframe) {
        // calculate average latency
        if (d->vframe.sts > 0) {
            int delay = getTime() - d->vframe.sts;
            d->latency.addLatency(delay);
            d->vframe.sts = -1;
        }
        d->mutex.unlock(); // finished using d->vframe
        d->newframe = false;
    }
}

int XVideoSink::setVideoConfiguration(const VideoConfiguration &config) {
    if (d->config.color_format == 0) return -1;

    if (config.color_format & d->color_formats) {
        d->config.color_format = config.color_format;
        return 0;
    } else
    {
        switch (config.color_format) {
        case RGB32:
            if (d->color_formats & YUY2) {
                d->config.color_format = YUY2;
                return 1;
            }
            if (d->color_formats & YV12) {
                d->config.color_format = YV12;
                return 1;
            }
            break;
        case YUY2:
            if (d->color_formats & RGB32) {
                d->config.color_format = RGB32;
                return 1;
            }
            if (d->color_formats & YV12) {
                d->config.color_format = YV12;
                return 1;
            }
            break;
        case YV12:
            if (d->color_formats & YUY2) {
                d->config.color_format = YUY2;
                return 1;
            }
            if (d->color_formats & RGB32) {
                d->config.color_format = RGB32;
                return 1;
            }
            break;
        }
    }
    return -1;
}

const VideoConfiguration& XVideoSink::videoConfiguration() {
    return d->config;
}

int XVideoSink::heightForWidth(int w) const {
    if (d->vframe.isEmpty()) return -1;

    float ratio = w/(float)d->vframe.aspect_ratio.width;
    return (int)(d->vframe.aspect_ratio.height * ratio);
}

void XVideoSink::resizeEvent(QResizeEvent *evt)
{
    d->config.width = evt->size().width();
    d->config.height = evt->size().height();
}

int XVideoSink::latency()
{
    return d->latency.average();
}


} // namespace

#include "xv_sink.moc"
