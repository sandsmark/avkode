/*  AvKode: Qt4 Video-sink

    Copyright (C) 2006 Allan Sandfeld Jensen <sandfeld@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Steet, Fifth Floor,
    Boston, MA 02110-1301, USA.
*/

#include "qt4_sink.h"
#include "avframe.h"
#include "avtime.h"

#include <QtGui/QImage>
#include <QtGui/QPainter>
#include <QtCore/QMutex>
#include <QtCore/QDebug>


namespace AvKode {

template<int i>
struct LatencyCalculator {
    LatencyCalculator() : number(0), index(0), total(0){};
    void addLatency(int lat) {
        if (number < i) {
            number += 1;
            total += lat;
        } else {
            total -= latency[index];
            total += lat;
        }
        latency[index] = lat;
        index += 1;
        if (index == i) index = 0;
    }
    int average() {
        if (number > 0)
            return total /number;
        else
            return 0;
    }
    int latency[i];
    int number;
    int index;
    int total;
};

struct Qt4VideoSink::private_data {
/*
    QPixmap p1;
    QPixmap p2; */
    QImage image;

    QMutex mutex;

    Time frameTime;
    LatencyCalculator<8> latency;

    VideoConfiguration config;
};

Qt4VideoSink::Qt4VideoSink(QWidget *parent) : QWidget(parent)
{
    d = new private_data;
}

Qt4VideoSink::~Qt4VideoSink()
{
    delete d;
}

bool Qt4VideoSink::open()
{
    setVideoConfiguration();
//     show();

    connect(this, SIGNAL(doUpdate()), this, SLOT(update()));
    return true;
}

void Qt4VideoSink::close()
{
//     hide();
    d->image = QImage();
    disconnect();
}

void Qt4VideoSink::setVideoConfiguration()
{
    d->config.width = size().width();
    d->config.height = size().height();
    d->config.aspect_ratio.width = size().width();
    d->config.aspect_ratio.height = size().height();

    d->config.color_format = RGB32;
}

int Qt4VideoSink::setVideoConfiguration(const VideoConfiguration &config)
{
    setVideoConfiguration();

    if (config == d->config)
        return 0;
    else
        return 1;
}

const VideoConfiguration& Qt4VideoSink::videoConfiguration()
{
    return d->config;
}


bool Qt4VideoSink::writeVideoFrame(VideoFrame *vFrame)
{

    d->mutex.lock();
    d->frameTime = vFrame->sts;

    QImage vImage(vFrame->data, vFrame->width, vFrame->height, QImage::Format_RGB32);

    // Scale up to aspect ratio
    float wratio = vFrame->width/(float)vFrame->aspect_ratio.width;
    float hratio = vFrame->height/(float)vFrame->aspect_ratio.height;
    float ratio = qMax(wratio, hratio);
    QSize frame_size(vFrame->aspect_ratio.width*ratio,
                     vFrame->aspect_ratio.height*ratio);
    // resize to widget
    frame_size.scale(size(), Qt::KeepAspectRatio);
    d->image = vImage.scaled(frame_size, Qt::IgnoreAspectRatio, Qt::FastTransformation);
    d->mutex.unlock();

    emit doUpdate();

    return true;
}

void Qt4VideoSink::paintEvent(QPaintEvent *)
{
    if (d->image.isNull()) {
        qWarning() << "lulz";
        return;
    } else
        qWarning() << "lawl";

    d->mutex.lock();
    QPainter p(this);
    p.drawImage(QPoint(0,0), d->image /*,  Qt::ColorOnly | Qt::OrderedDither*/);

    QFont font("Arial", 16);
    p.setPen(Qt::white);
    p.setFont(font);
    p.drawText(QPoint(20,20), QString("Delay: ")+QString::number(latency())+"ms");

    // ### does drawImage block 'till the image is on the X11 server?
    // if not we need to add X11 latency for network transparency
    if (d->frameTime != 0) {
        int delay = getTime() - d->frameTime;
        d->latency.addLatency(delay);
        d->frameTime = 0;
    }
    d->mutex.unlock();
}

int Qt4VideoSink::latency()
{
    return d->latency.average();
}

/*
#define RGB_BGR(x) (((x&0xff0000)>>16) | ((x&0xff)<<16) | (x&0xff00ff00))
// convert from RGB24 to Qt-RGB32
static void convert(AvKode::VideoFrame* vFrame, uchar *vdata)
{
    unsigned int len = vFrame->width*vFrame->height;
    unsigned int *indata = (unsigned int*)vFrame->data;
    unsigned int *img = (unsigned int*)vdata;

    unsigned int rlen = (len/4)*4;
    unsigned int j = 0;
    unsigned int i, in, rgb, old;
#ifdef BE
    for(i= 0; i<rlen; i+=4) {
        {
            in = indata[j++];
            rgb = (in & 0xffffff00) >> 8;
            rgb = rgb | 0xff000000;
            old = in & 0xff;
            img[i] = rgb;
        }
        {
            in = indata[j++];
            rgb = (in & 0xffff0000) >> 16;
            rgb = rgb | (old << 16) | 0xff000000;
            old = in & 0xffff;
            img[i+1] = rgb;
        }
        {
            in = indata[j++];
            rgb = (in & 0xff000000) >> 24;
            rgb = rgb | (old << 8) | 0xff000000;
            old = in & 0xffffff;
            img[i+2] = rgb;
        }
        {
            rgb = old | 0xff000000;
            img[i+3] = rgb;
        }
    }
#else
    for(i= 0; i<rlen; i+=4) {
        {
            in = indata[j++];
            rgb = (in & 0x00ffffff);
            rgb = rgb | 0xff000000;
            old = in & 0xff000000;
            img[i] = RGB_BGR(rgb);
        }
        {
            in = indata[j++];
            rgb = (in & 0x0000ffff);
            rgb = (rgb << 8) | (old >> 24) | 0xff000000;
            old = in & 0xffff0000;
            img[i+1] = RGB_BGR(rgb);
        }
        {
            in = indata[j++];
            rgb = (in & 0x000000ff);
            rgb = (rgb << 16) | (old >> 16) | 0xff000000;
            old = in & 0xffffff00;
            img[i+2] = RGB_BGR(rgb);
        }
        {
            rgb = (old >> 8) | 0xff000000;
            img[i+3] = RGB_BGR(rgb);
        }
    }
#endif
    for(; i<len; i++) {
        int red   = vFrame->data[i*3 + 0];
        int green = vFrame->data[i*3 + 1];
        int blue  = vFrame->data[i*3 + 2];
        img[i] = qRgb(red,green,blue);
    }
}
*/

} // namespace

#include "qt4_sink.moc"
