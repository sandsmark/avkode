/*  This file is part of the KDE project
    Copyright (C) 2006 Allan Sandfeld Jensen (kde@carewolf.com)

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301, USA.

*/

#include "kiostream.h"
#include <kurl.h>
#include <kio/job.h>
#include <kio/filejob.h>

#include "bytebuffer.h"
#include "bytestream.h"

#include <iostream>
#include <assert.h>

struct KIOStream::private_data {
    private_data() : job(0), requested(0) {}

    KIO::FileJob* job;
    KIO::filesize_t position;
    KIO::filesize_t length;
    KIO::filesize_t seek;
    long requested;
    AvKode::ByteBuffer *buffer;
    QByteArray blob;
    KUrl url;
    bool half_open;
    bool open;
    bool seeking;
    bool suspend;
};

KIOStream::KIOStream(AvKode::ByteBuffer *buffer)
{
    d = new private_data;
    d->buffer = buffer;
}

KIOStream::~KIOStream() {
    if (d->job) closeStream();
    delete d;
}

bool KIOStream::openStream(const KUrl& url)
{
    if (d->job) return false;

    d->job = KIO::open(url, QIODevice::ReadOnly);
    d->position = 0;
    d->seek = 0;
    d->seeking = false;
    d->open = false;
    d->suspend = false;
    d->requested = 0;
    // connect stuff
    connect(d->job, SIGNAL(open(KIO::Job*)), this, SLOT(slotOpen(KIO::Job*)));
    connect(d->job, SIGNAL(close(KIO::Job*)), this, SLOT(slotClose(KIO::Job*)));
    connect(d->job, SIGNAL(data(KIO::Job*, const QByteArray&)), this, SLOT(slotData(KIO::Job*, const QByteArray&)));
    connect(d->job, SIGNAL(position(KIO::Job*, KIO::filesize_t)), this, SLOT(slotPosition(KIO::Job*, KIO::filesize_t)));

    connect(d->job, SIGNAL(result(KJob*)), this, SLOT(slotResult(KJob*)));

    return true;
}

void KIOStream::closeStream() {
    d->job->close();
    disconnect();
    delete d->job;
    d->job = 0;
}

const char* KIOStream::filename() const {
    return d->url.url().toLatin1();
}

void KIOStream::resume() { d->suspend = false; }
void KIOStream::suspend() { d->suspend = true; }
uint64_t KIOStream::length() { return 0; }
uint64_t KIOStream::position() { return d->position; }

void KIOStream::seek(uint64_t pos)
{
    assert(d->open);
    std::cout << "KIOStream::seek: " << pos << std::endl;
    if (pos == d->position) return;
    d->seek = pos; // discard until pos
    d->seeking = true;
    d->job->seek(pos);
}

void KIOStream::requestData(int size)
{
    if (!d->open) {
        d->requested += size;
        return;
    }
//     std::cout << "KIOStream::requestData(" << size << ")\n";
    if (d->suspend) {
        if (d->buffer->space() >= (unsigned int)d->blob.size()) {
            d->buffer->write((char*)d->blob.data(), d->blob.size(), false);
            d->blob.clear();
            resume();
        } else
            return;
    }
    d->job->read(size);
}

void KIOStream::slotData(KIO::Job *, const QByteArray &data)
{
//     assert( job == d->job );
    if (d->seeking) {
        std::cout << "Discarding data - seeking: " << d->seek << std::endl;
        return;
    }
    if (!d->open) {
        std::cout << "Discarding data - closed! " << std::endl;
        return;
    }
    if (d->suspend) {
        std::cout << "Data during suspend!" << std::endl;
        d->blob.append(data); // ### thread-safety?
        return;
    }
    std::cout << "KIOStream::slotData(" << data.size() << ")\n";

    if (data.size() == 0) {
        d->buffer->end();
        return;
    }

    long freespace = d->buffer->space();
    // write non-blocking..
    if (freespace >= data.size())
        d->buffer->write((char*)data.data(), data.size(), false);
    else {
        std::cout << "suspend job" << std::endl;
        suspend();
        d->blob = data;
    }
}

void KIOStream::slotPosition(KIO::Job *, KIO::filesize_t offset)
{
    std::cout << "KIOStream::slotPosition( " << offset << ")\n";
//     assert( job == d->job );
    if (d->seeking) {
        if (offset != d->seek)
            std::cout << "Seeked failed? " << offset << "!=" << d->seek << std::endl;
        else
            d->seeking = false;
    } else
        if (offset != 0)
            std::cout << "Not seeking???" << std::endl;
    d->position = offset;
}

// We can now issue inline commands
void KIOStream::slotOpen(KIO::Job *)
{
    std::cout << "KIOStream::slotOpen()\n";
    d->length = d->job->size();
    d->open = true;
    d->buffer->start();

    if (d->requested > 0)
        d->job->read(d->requested);
}

// We can now issue inline commands
void KIOStream::slotClose(KIO::Job *)
{
    std::cout << "KIOStream::slotClose()\n";
    d->open = false;
    d->buffer->end();
}

// We can now issue inline commands
void KIOStream::slotResult(KJob *)
{
    std::cout << "KIOStream::slotResult()\n";
    d->open = false;
}

#include "kiostream.moc"
