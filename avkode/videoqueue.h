/*  AvKode: Video Queue

    Copyright (C) 2006 Allan Sandfeld Jensen <kde@carewolf.com>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Steet, Fifth Floor,
    Boston, MA 02110-1301, USA.
*/

#ifndef _AVKODE_VIDEOQUEUE_H
#define _AVKODE_VIDEOQUEUE_H

namespace AvKode {

class VideoSink;
class VideoFrame;

class VideoQueue {
public:
    VideoQueue();
    ~VideoQueue();

    void open(VideoSink *sink);
    void close();

    // Insert event at delay ms in the future
    void delayVideoFrame(int delay, VideoFrame *frame);

    struct private_data;
private:
    private_data *d;
};

} // namespace

#endif
