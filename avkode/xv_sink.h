/*  AvKode: Xv Video-sink

    Copyright (C) 2006 Allan Sandfeld Jensen <kde@carewolf.com>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Steet, Fifth Floor,
    Boston, MA 02110-1301, USA.
*/

#ifndef AVKODE_XV_SINK_H
#define AVKODE_XV_SINK_H

#include "avconfiguration.h"
#include "sink.h"
#include <QtGui/QWidget>
#include <kdemacros.h>

namespace AvKode {

class KDE_EXPORT XVideoSink : public QWidget, public VideoSink
{
    Q_OBJECT
public:
    XVideoSink(QWidget *parent=0);
    ~XVideoSink();

    bool open();
    void close();

    bool writeVideoFrame(VideoFrame *frame);

    int setVideoConfiguration(const VideoConfiguration &config);
    const VideoConfiguration& videoConfiguration();

    int latency();

// QWidget call:
    int heightForWidth(int w) const;

private:
    bool checkXv();

    struct private_data;
    private_data *d;

signals:
    void doUpdate();

protected:
    void paintEvent(QPaintEvent *evt);
    void resizeEvent(QResizeEvent *evt);
};

} // namespace;

#endif
