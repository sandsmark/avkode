/*  AvKode: Audio Queue

    Copyright (C) 2006 Allan Sandfeld Jensen <kde@carewolf.com>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Steet, Fifth Floor,
    Boston, MA 02110-1301, USA.
*/

#include "audioqueue.h"
#include "avpool.h"
#include "avtime.h"
#include "thread.h"
#include "sink.h"
#include "avframe.h"

#include <assert.h>
#include <queue>
#include <iostream>

using std::queue;

namespace AvKode {

struct AudioQueue::private_data {
    private_data() : sink(0), minimum_delay(200), firstpos(-1), lastpos(-1) {}

    AudioSink *sink;

    AvFramePool pool;

    int minimum_delay;
    long firstpos;
    long lastpos;

    // Replace with specialized threadsafe queue
    queue<AudioFrame*> frame_queue;
};


AudioQueue::AudioQueue() {
    d = new private_data;
}

AudioQueue::~AudioQueue() {
    if (d->sink) close();
    delete d;
}

void AudioQueue::open(AudioSink *sink) {
    d->sink = sink;
}

void AudioQueue::close() {
    d->sink = 0;

    // free vframes
    while (!d->frame_queue.empty()) {
        delete d->frame_queue.front();
        d->frame_queue.pop();
    }
}

int AudioQueue::minimumDelay() {
    return d->minimum_delay;
}

void AudioQueue::setMinimumDelay(int delay) {
    d->minimum_delay = delay;
}

void AudioQueue::writeAudioFrame(AudioFrame *frame)
{
    // We can't take over frame, so we take over its content
    AudioFrame *pframe = d->pool.getAudioFrame();
    swapAudioFrames(pframe, frame);

    d->lastpos = pframe->pos;
    if (d->frame_queue.empty()) d->firstpos = pframe->pos;
    d->frame_queue.push(pframe);

    if (d->lastpos - d->firstpos > d->minimum_delay) {
        AudioFrame *aframe = d->frame_queue.front();
        d->sink->writeAudioFrame(aframe);
        d->pool.putAudioFrame(aframe);
        d->frame_queue.pop();
        if (!d->frame_queue.empty())
            d->firstpos = d->frame_queue.front()->pos;
    }
}


} // namespace
