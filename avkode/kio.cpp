/*  AvKode: KIO bytestream type

    Copyright (C) 2006 Allan Sandfeld Jensen <kde@carewolf.com>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Steet, Fifth Floor,
    Boston, MA 02110-1301, USA.
*/

#include "kio.h"
#include "kiostream.h"
#include "bytebuffer.h"
#include "bytestream.h"

#include <iostream>

namespace AvKode {

namespace KIO {
    class ByteStream;
}

class KIO::ByteStream : public AvKode::ByteStream {
    AvKode::ByteBuffer* m_buffer;
    KIOStream *m_stream;
    bool m_open, m_seekable;
    long m_pos;
    long m_len;
    bool m_started;
public:
    ByteStream(KIOStream *kstream, AvKode::ByteBuffer* buffer) :
        m_buffer(buffer),
        m_stream(kstream),
        m_seekable(false),
        m_pos(-1), m_len(-1),
        m_started(false)
    {}
    virtual ~ByteStream() {
        m_stream->closeStream();
        m_buffer->release();
        delete m_stream;
        delete m_buffer;
    }
    const char* filename() const {
        return m_stream->filename();
    }
    long read(char* ptr, unsigned long num) {
        if (num<=0) return 0;

        std::cout << "read(" << num << ")" << std::endl;
        m_stream->requestData(num);
        long n = m_buffer->read(ptr,num,false);
        m_pos += n;

        std::cout << "/read(" << n << ")" << std::endl;;
        return n;
    }
    bool seek(long to, int whence) {
        long newpos = 0;
        switch (whence) {
            case SEEK_SET:
                newpos = to;
                break;
            case SEEK_CUR:
                newpos = m_pos + to;
                break;
            case SEEK_END:
                if (m_len < 0) return false;
                newpos = m_len + to;
                break;
            default:
                return false;
        }
        std::cout << "Seeking to " << newpos << std::endl;
        m_stream->seek(newpos);
        m_buffer->flush();
        m_pos = newpos;
        return true;
    }
    long available() const {
        return m_buffer->content();
    }
    long position() const {
        return m_pos;
    }
    long length() const {
        return m_len;
    }
    bool seekable() const { return m_seekable; }
    long waitFor(unsigned long len) {
        unsigned long content = m_buffer->content();
        if (content < len) {
            m_stream->requestData(len - content);
            return m_buffer->waitFor(len);
        }
        return content;
    }
    void setBufferSize(unsigned long len) {
        m_buffer->setBufferSize(len);
    }
    unsigned long bufferSize() const {
        return m_buffer->bufferSize();
    }
    void release() {
        m_buffer->release();
        m_stream->suspend();
    }
    void reset() {
        m_buffer->reset();
        m_stream->seek(0);
        m_stream->resume();
    }
    bool eof() const { return m_buffer->eof(); }
    bool error() const { return m_buffer->error(); }

};

ByteStream* KIO::open(const QUrl& url) {
    ByteBuffer *buffer = new ByteBuffer(1<<20);
    KIOStream *kstream = new KIOStream(buffer);
    kstream->openStream(url);
    ByteStream *stream = new KIO::ByteStream(kstream, buffer);

    return stream;
}

} // namespace
