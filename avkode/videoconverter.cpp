/*  AvKode: VideoConverter

    Copyright (C) 2006 Allan Sandfeld Jensen <kde@carewolf.com>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Steet, Fifth Floor,
    Boston, MA 02110-1301, USA.
*/
#define __STDC_CONSTANT_MACROS
extern "C" {
#include <stdint.h>
#include <libavcodec/avcodec.h>
#include <libswscale/swscale.h>
}

#include "avframe.h"
#include "videoconverter.h"

#include <stdlib.h>


namespace AvKode {

VideoConverter::VideoConverter(ColorFormat color_format) : m_color_format(color_format) {}

static PixelFormat pixelFormat(ColorFormat color_format) {
    switch (color_format) {
    case RGB32:
        return PIX_FMT_RGB32;
    case YUY2:
        return PIX_FMT_YUYV422;
    case YV12:
        return PIX_FMT_YUV420P;
    default:
        assert(false);
        return PIX_FMT_NONE;
    }
}

bool VideoConverter::doFrame(VideoFrame* in, VideoFrame *out)
{
    if (in->color_format == m_color_format) {
        swapVideoFrames(in, out);
        return true;
    }

    VideoConfiguration config = *in;
    config.color_format = m_color_format;
    out->reserveSpace(config);

    {
        PixelFormat out_fmt = pixelFormat(m_color_format);
        PixelFormat in_fmt = pixelFormat((ColorFormat)in->color_format);
        AVFrame *frameIn, *frameOut;

        //TODO: don't recreate this
        SwsContext *scaleContext = sws_getContext(in->width, in->height, in_fmt, 
                out->width, out->height, out_fmt,
                SWS_BICUBIC, NULL, NULL, NULL);
        
        frameIn=avcodec_alloc_frame();
        frameOut=avcodec_alloc_frame();

        avpicture_fill((AVPicture *)frameIn, in->data, in_fmt, in->width, in->height);
        avpicture_fill((AVPicture *)frameOut, out->data, out_fmt, out->width, out->height);

        //img_convert((AVPicture *)frameOut, out_fmt, (AVPicture*)frameIn, in_fmt, in->width, in->height);
        sws_scale(scaleContext, frameIn->data,
                frameIn->linesize, 0, 
                frameIn->height, 
                frameOut->data, frameOut->linesize);
        
        sws_freeContext(scaleContext);

        free ( frameIn );
        free ( frameOut );
    }

    return true;
}

void VideoConverter::setColorFormat(ColorFormat color_format)
{
    m_color_format = color_format;
}

} // namespace

