/*  AvKode: FFMPEG Decoder

    Copyright (C) 2005,2006 Allan Sandfeld Jensen <kde@carewolf.com>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Steet, Fifth Floor,
    Boston, MA 02110-1301, USA.
*/

// #ifdef HAVE_FFMPEG

#define __STDC_CONSTANT_MACROS
extern "C" {
#include <stdint.h>
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libavformat/avio.h>
#include <libswscale/swscale.h>
}
#include "file.h"
#include "avframe.h"
#include "bytestream.h"
// #include "reinterlace.h"

#include <assert.h>

#include "ffmpeg_decoder.h"
#include <iostream>

// FFMPEG callbacks
static int avkode_read(void* opaque, unsigned char *buf, int size)
{
    AvKode::ByteStream *file = (AvKode::ByteStream*)opaque;
    file->waitFor(size);
    return file->read((char*)buf, size);
}
#if 0
    static int avkode_write(void* opaque, unsigned char *buf, int size)
    {
        AvKode::ByteStream *file = (AvKode::ByteStream*)opaque;
        return file->write((char*)buf, size);
    }
#endif
static int64_t avkode_seek(void* opaque, int64_t pos, int whence)
{
    AvKode::ByteStream *file = (AvKode::ByteStream*)opaque;
    file->seek(pos, whence);
    return file->position();
}


namespace AvKode {

    // FIXME? this size
#define FILE_BUFFER_SIZE 32768

// Known stream types
enum StreamType { NoStream, MPEG1, MPEG2, AVI, Other };

struct FFMPEGDecoder::private_data
{
    private_data() : formatContext(0), audioCodec(0), videoCodec(0), frame(0), scaleContext(0),
                     audioStream(-1), videoStream(-1), packetSize(0),
                     packetType(NoFrameType), nextType(NoFrameType), lastType(NoFrameType),
                     length(-1), position(0), frame_pos(0), frame_nr(0), pts_scew(0), aoffset(0), pts_correct(0),
                     last_vpos(0), last_vdur(0),
                     eof(false), error(false), initialized(false), retries(0), file_buffer(0), buffer_size(0)
    {
        frame = avcodec_alloc_frame();
    }
    ~private_data() {
        free(frame);
    }

    AVFormatContext* formatContext;
    AVCodec* audioCodec;
    AVCodec* videoCodec;
    AVFrame* frame;
    SwsContext *scaleContext;

    int audioStream;
    int videoStream;
    StreamType stream_type;

    AVPacket packet;
    uint8_t* packetData;
    int packetSize;
    FrameType packetType;
    FrameType nextType;
    FrameType lastType;
    int picture;

    AudioFrame audioFrame;
    VideoFrame videoFrame;
    VideoFrame interlacedFrame;

    int interlaced;
    bool topFirst;
//     ReInterlace reinterlace;

    ByteStream *src;
    AudioConfiguration audio_config;
    VideoConfiguration video_config;

    long length;
    long position;  // position in audio-stream
    long frame_pos; // pos of current GOP
    int frame_nr;   // number in current GOP
    long pts_scew;
    long aoffset;   // Audio offset in AVI files
    long pts_correct;

    long last_vpos;
    long last_vdur;

    bool eof, error;
    bool initialized;
    int retries;

    unsigned char *file_buffer;
    char buffer[AVCODEC_MAX_AUDIO_FRAME_SIZE];
    int buffer_size;
};

FFMPEGDecoder::FFMPEGDecoder() {
    d = new private_data;
    av_register_all();
}

FFMPEGDecoder::~FFMPEGDecoder() {
    if (d->src) close();
    delete d;
}


static bool setAudioConfiguration(AudioConfiguration *config, AVCodecContext *codec_context)
{
    config->sample_rate = codec_context->sample_rate;
    config->channels = codec_context->channels;
    if (config->channels <= 2)
        config->channel_config = MonoStereo;
    else
        // FFMPEG have no surround channel ordering!
        config->channel_config = MultiChannel;

    // avcodec.h says sample_fmt is not used. I guess it means it is always S16
    switch(codec_context->sample_fmt) {
        case AV_SAMPLE_FMT_U8:
            config->sample_width = 8; // beware unsigned!
            break;
        case AV_SAMPLE_FMT_S16:
            config->sample_width = 16;
            break;
        case AV_SAMPLE_FMT_S32:
            config->sample_width = 32;
            break;
        case AV_SAMPLE_FMT_FLT:
            config->sample_width = -32;
            break;
        default:
            return false;
     }
     return true;
}

static PixelFormat pixelFormat(ColorFormat color_format) {
    switch (color_format) {
    case RGB32:
        return PIX_FMT_RGB32;
    case YUY2:
        return PIX_FMT_YUYV422;
    case YV12:
        return PIX_FMT_YUV420P;
    default:
        assert(false);
        return PIX_FMT_NONE;
    }
}

static bool setVideoConfiguration(VideoConfiguration *config, AVCodecContext *codec_context)
{
    config->width = codec_context->width;
    config->height = codec_context->height;

    if (codec_context->sample_aspect_ratio.num > 0) {
        // calculate from stupid relative aspect ratio to real aspect ratio
        AVRational sample_size;
        sample_size.num = codec_context->width; sample_size.den = codec_context->height;
        AVRational aspect_ratio = av_mul_q(sample_size, codec_context->sample_aspect_ratio);

        config->aspect_ratio.width = aspect_ratio.num;
        config->aspect_ratio.height = aspect_ratio.den;
    } else {
        config->aspect_ratio.width = config->width;
        config->aspect_ratio.height = config->height;
    }

    switch (codec_context->pix_fmt) {
    case PIX_FMT_YUV422P:
    case PIX_FMT_YUVJ422P:
    case PIX_FMT_YUV411P:
    case PIX_FMT_YUYV422:
    case PIX_FMT_UYVY422:
        config->color_format = YUY2;
        break;
    case PIX_FMT_YUVJ420P:
    case PIX_FMT_YUV420P:
    case PIX_FMT_YUV410P:
        config->color_format = YV12;
        break;
    case PIX_FMT_YUV444P:
    case PIX_FMT_YUVJ444P:
    case PIX_FMT_BGR24:
    case PIX_FMT_RGB24:
    case PIX_FMT_RGB32:
    case PIX_FMT_RGB565:
    case PIX_FMT_RGB555:
        config->color_format = RGB32;
        break;
    default:
        return false;
    }
    std::cout << "avkode: FFMPEG: Frame size: " << config->width << "/" << config->height << "\n";
    std::cout << "avkode: FFMPEG: Aspect ratio: " << config->aspect_ratio.width << "/"
                                                 << config->aspect_ratio.height << "\n";
    return true;
}

bool FFMPEGDecoder::open(ByteStream *src) {
    d->src = src;

    d->formatContext = avformat_alloc_context();
    d->buffer_size = FILE_BUFFER_SIZE;
    d->file_buffer = reinterpret_cast<unsigned char*>(av_malloc(d->buffer_size));
    d->formatContext->pb = avio_alloc_context( d->file_buffer,
        d->buffer_size, 0, src, avkode_read, NULL, avkode_seek);
    d->formatContext->pb->seekable = seekable();

    int err = avformat_open_input(&d->formatContext, d->src->filename(), 0, 0);
    if (err != 0) {
        char errbuf[1024];
        av_strerror(err, errbuf, 1024);
        std::cerr << "avkode: FFMPEG: error opening: " << errbuf << std::endl;
        close();
        return false;
    }


    avformat_find_stream_info(d->formatContext, 0);
    //av_find_stream_info( d->formatContext );

    std::cout << "avkode: FFMPEG: Format found: " << d->formatContext->iformat->name << "\n";

    // mpeg and avi are crap
    if (!strcmp(d->formatContext->iformat->name,"mpeg")) d->stream_type = MPEG1;
    else
    if (!strcmp(d->formatContext->iformat->name,"avi")) d->stream_type = AVI;
    else
        d->stream_type = Other;

    // Find the first a/v streams
    d->audioStream = -1;
    d->videoStream = -1;
    for (unsigned int i = 0; i < d->formatContext->nb_streams; i++) {
        if (d->formatContext->streams[i]->codec->codec_type == AVMEDIA_TYPE_AUDIO) {
            d->audioStream = i;
        } else if (d->formatContext->streams[i]->codec->codec_type == AVMEDIA_TYPE_VIDEO) {
            d->videoStream = i;
        }
    }

    if (d->audioStream == -1)
    {
        std::cerr << "avkode: FFMPEG: Audio stream not found\n";
        // for now require an audio stream
        close();
        return false;
    }

    // Set config
    if (!setAudioConfiguration(&d->audio_config, d->formatContext->streams[d->audioStream]->codec))
    {
        std::cerr << "avkode: FFMPEG: Unable to set audio configuration\n";
        close();
        return false;
    }
    if (d->videoStream != -1)
        if (!setVideoConfiguration(&d->video_config, d->formatContext->streams[d->videoStream]->codec))
        {
            std::cerr << "avkode: FFMPEG: Unable to set video configuration\n";
            close();
            return false;
        }


/*
    if (d->formatContext->start_time != (int64_t)AV_NOPTS_VALUE) {
        double ffpos = (double)d->formatContext->start_time / (double)AV_TIME_BASE;
        d->position = (long)(ffpos * d->audio_config.sample_rate);
        std::cout << "Start time " << ffpos << std::endl;
    } else */
        d->position = 0;
    if (d->formatContext->duration != (int64_t)AV_NOPTS_VALUE) {
        double fflen = (double)d->formatContext->duration / (double)AV_TIME_BASE;
        d->length = (long)(fflen * 1000.0);
        std::cout << "avkode: FFMPEG: Length " << fflen << std::endl;
    } else
        d->length = -1;

    d->audioCodec = avcodec_find_decoder(d->formatContext->streams[d->audioStream]->codec->codec_id);
    if (!d->audioCodec) {
        std::cerr << "avkode: FFMPEG: Audio codec not found\n";
        close();
        return false;
    } else
        std::cout << "avkode: FFMPEG: Audio codec: " << d->audioCodec->name << "\n";
    if (d->videoStream != -1) {
        d->videoCodec = avcodec_find_decoder(d->formatContext->streams[d->videoStream]->codec->codec_id);
        if (!d->videoCodec) {
            std::cerr << "avkode: FFMPEG: Video codec not found\n";
            d->audioCodec = 0;
            close();
            return false;
        } else {
            std::cout << "avkode: FFMPEG: Video codec: " << d->videoCodec->name << "\n";
            if (d->stream_type == MPEG1 && strncmp(d->videoCodec->name, "mpeg2", 5) == 0)
                d->stream_type = MPEG2;
        }
    }
    avcodec_open2(d->formatContext->streams[d->audioStream]->codec, d->audioCodec, 0);
    if (d->videoCodec)
        avcodec_open2(d->formatContext->streams[d->videoStream]->codec, d->videoCodec, 0);

    d->initialized = true;

    return true;
}

void FFMPEGDecoder::close() {
    if( d->packetSize > 0 ) {
        av_free_packet( &d->packet );
        d->packetSize = 0;
    }

    if( d->audioCodec ) {
        avcodec_close( d->formatContext->streams[d->audioStream]->codec );
        d->audioCodec = 0;
    }
    if( d->videoStream != -1 && d->videoCodec ) {
        avcodec_close( d->formatContext->streams[d->videoStream]->codec );
        d->videoCodec = 0;
    }
    if( d->formatContext ) {
        // make sure av_close_input_file doesn't actually close the file
        d->formatContext->iformat->flags = d->formatContext->iformat->flags | AVFMT_NOFILE;
        avformat_close_input(&d->formatContext);
        d->formatContext = 0;
    }
    
    if (d->scaleContext) {
        sws_freeContext(d->scaleContext);
        d->scaleContext = 0;
    }

    d->src = 0;
}

static long audioPosition(long pos, int sample_rate) {
    int div = pos / sample_rate;
    int rem = pos % sample_rate;
    return div * 1000L + (rem * 1000L) / sample_rate;
}

template<typename T>
static long demux(FFMPEGDecoder::private_data* d, AudioFrame* frame) {
    int channels = d->audio_config.channels;
    //long length = d->buffer_size/(channels*sizeof(T));
    long length = d->frame->nb_samples;
    frame->reserveSpace(&d->audio_config, length);

    T offset = 0;
    if (frame->sample_width == 8) offset = -128; // convert unsigned to signed

    // Demux into frame
    T* buffer = (T*)d->frame->data[0];
//     T** data = (T**)frame->data;
    T** data = (T**)frame->data;
//    int bufsize = av_samples_get_buffer_size(NULL, channels, d->frame->nb_samples, (AVSampleFormat) d->frame->format, 0);
//      std::cerr << " length: " << length << " channels: " << channels << " keke: " <<  << "\n";
    for(int i=0; i<length; i++)
        for(int j=0; j<channels; j++) {
//            assert(i*channels+j < bufsize);
//             std::cerr << "channel: " << j << " i: " << i << "\n";
//             data[j][i] = d->frame->data[0][i*channels+j];//buffer[i*channels+j] + offset;
            data[j][i] = buffer[i*channels+j] + offset;
        }
//     for (int i=0; i<bufsize; i++) {
//         data[i%2][i/2] = (float)d->frame->data[i];
//     }

    // Set pos
    /*
    if (d->packet->pts != (int64_t)AV_NOPTS_VALUE && d->packet->pts != 0) {
        AVRational timebase = d->formatContext->streams[d->audioStream]->codec->time_base;
        int64_t pts = d->frame->pts;
        int msecs = (pts * 1000 * timebase.num) / timebase.den;
        frame->pos = msecs + (d->sample_pos *1000) / d->audio_config.sample_rate;
    }
    else */
    frame->pos = audioPosition(d->position, d->audio_config.sample_rate);

    d->position += length;

    return length;
}

static void sync_interleave(FFMPEGDecoder::private_data* d, VideoFrame* frame)
{
        // MPEG-1 and Divx 3.11 ;)
        // have stable interleave, but no or useless timestamps
        AVCodecContext *codecContext = d->formatContext->streams[d->videoStream]->codec;
        AVRational timebase = codecContext->time_base;

        unsigned long msecs = (d->frame_nr * 1000UL * timebase.num) / timebase.den;
        // fps is notiously unreliable, so adjust according to interleaved audio position
        if (d->lastType == AudioFrameType && d->audio_config.sample_rate > 0)  {
            long apos = audioPosition( d->position, d->audio_config.sample_rate) - d->aoffset;
            int diff = apos -(msecs+ d->frame_pos);
            if (diff < -100 || diff > 100) {
                std::cout << "Sync on interleave " << diff <<  "ms\n";
                d->frame_pos = apos;
            } else
                d->frame_pos = (msecs + d->frame_pos);
            d->frame_nr = 0;
            msecs = 0;
        }

        long syncpos = d->frame_pos + msecs;
        long minsync = d->last_vpos + (3*d->last_vdur)/4;
        long maxsync = d->last_vpos + (4*d->last_vdur)/3;
        if (syncpos < minsync) syncpos = minsync;
        if (syncpos > maxsync) syncpos = maxsync;

        frame->pos = syncpos;
        frame->duration = (1000 * timebase.num) /timebase.den;
        d->last_vpos = frame->pos;
        d->last_vdur = frame->duration;
}

static void sync_fps(FFMPEGDecoder::private_data* d, VideoFrame* frame)
{
        // MPEG2 has no stable interleave
        // has no correct frame-pts and no correct packet-pts

        AVCodecContext *codecContext = d->formatContext->streams[d->videoStream]->codec;
        AVRational timebase = codecContext->time_base;

        unsigned long msecs = (d->frame_nr * 1000UL * timebase.num) / timebase.den;
        if (msecs > 65536) {
            d->frame_pos = msecs + d->frame_pos;
            d->frame_nr = 0;
            msecs = 0;
        }

        frame->pos = d->frame_pos + msecs;
        frame->duration = (1000 * timebase.num) /timebase.den;

}

static void sync_pts(FFMPEGDecoder::private_data* d, VideoFrame* frame)
{
        AVCodecContext *codecContext = d->formatContext->streams[d->videoStream]->codec;
        AVRational timebase = codecContext->time_base;
        // correct for initial pts
        if (d->frame_nr == 0 && d->frame->pts != 0)  {
            std::cout << "PTS off by " << d->frame->pts << std::endl;
            d->pts_scew = d->frame->pts;
            d->frame_pos = 0;
        }
        int64_t pts = d->frame->pts - d->pts_scew;
        int msecs = (pts * 1000 * timebase.num) / timebase.den;
        /*
        if (d->lastType == AudioFrameType && d->audio_config.sample_rate > 0) {
            long apos = audioPosition( d->position, d->audio_config.sample_rate) - d->aoffset;
            int diff = apos - (msecs + d->frame_pos);
            if (diff < -20 || diff > 20) {
                std::cout << "Sync on interleave " << diff <<  "ms\n";
                d->frame_pos = apos;
                d->pts_scew = d->frame->pts;
                msecs = 0;
            }
        }*/

        frame->pos = msecs + d->frame_pos;
        frame->duration = -1; // we need the next frame to actually know
        std::cout << "Sync on frame pts: " << pts << " pos: " << frame->pos << std::endl;
}

static void convert(FFMPEGDecoder::private_data* d, VideoFrame* frame) {
    AVCodecContext *codecContext = d->formatContext->streams[d->videoStream]->codec;
    
    frame->reserveSpace(d->video_config);

    AVFrame *pFrame = d->frame;
    /*
    if (d->frame->interlaced_frame) {
         pFrame = avcodec_alloc_frame();
//          uint8_t *buffer = avcodec_default_get_buffer(codecContext, pFrame);
         avpicture_alloc((AVPicture*)pFrame, codecContext->pix_fmt, codecContext->width, codecContext->height);
//          avpicture_fill((AVPicture*)pFrame, buffer, codecContext->pix_fmt, codecContext->width, codecContext->height);
         avpicture_deinterlace((AVPicture*)pFrame, (AVPicture*)d->frame,
                               codecContext->pix_fmt, codecContext->width, codecContext->height);
    }*/
    if (d->frame->interlaced_frame) {
        d->interlaced = 1;
        d->topFirst = (d->frame->top_field_first == 0);
    } else
        d->interlaced = 0;

    ColorFormat color_format = (ColorFormat)d->video_config.color_format;
    PixelFormat pix_fmt = pixelFormat(color_format);
//     if (pixelFormat(color_format) != codecContext->pix_fmt)
    {
        if (d->scaleContext == 0)
            d->scaleContext = sws_getContext(codecContext->width, codecContext->height, 
                        codecContext->pix_fmt, 
                        codecContext->width, codecContext->height, pix_fmt, SWS_BICUBIC, 
                        NULL, NULL, NULL);
        
        // convert data
        AVFrame *pFrameRGB;
        uint8_t *buffer;
        pFrameRGB=avcodec_alloc_frame();
        buffer=frame->data;
        avpicture_fill((AVPicture *)pFrameRGB, buffer, pix_fmt, codecContext->width, codecContext->height);

        //img_convert((AVPicture *)pFrameRGB, pix_fmt, (AVPicture*)pFrame,
        //    codecContext->pix_fmt, codecContext->width, codecContext->height);
        
        //int img_convert(AVPicture *dst, int dst_pix_fmt, const AVPicture *src, int pix_fmt, int width, int height)
        //int sws_scale(SwsContext *c, uint8_t *src, int srcStride[], int srcSliceY, int srcSliceH, uint8_t dst[], int dstStride[]

        sws_scale(d->scaleContext, pFrame->data, 
                pFrame->linesize, 0, 
                codecContext->height, 
                pFrameRGB->data, pFrameRGB->linesize);
        
        free ( pFrameRGB );

        if (pFrame != d->frame)
            avpicture_free((AVPicture*)pFrame);
    } /*else {
        // copy data
        memcpy(frame->data, d->frame->data[0], avpicture_get_size(codecContext->pix_fmt, codecContext->width, codecContext->height));
    }*/

    // Set pos and duration
    if (d->frame_nr == 0 && d->frame->pts == 0 && d->audio_config.sample_rate > 0) {
        d->aoffset = audioPosition(d->position, d->audio_config.sample_rate);
        std::cout << "audio offset " << d->aoffset << std::endl;
    }

    // Okay so we need to treat everything differently because FFMPEG does
    switch(d->stream_type) {
    case MPEG1:
        sync_interleave(d, frame);
        break;
    case MPEG2: {
        sync_fps(d, frame);
        break;
    }
    case AVI:
    case Other: {
        AVRational timebase = codecContext->time_base;
//         std::cout << "Timebase is: " << timebase.num << "/" << timebase.den << "\n";
        // check for fps-like timebase
        if ((timebase.num*100)/timebase.den > 0) {
            if (d->frame->pts == 0)
                sync_interleave(d, frame);
            else
                sync_fps(d, frame);
        }
        else
            sync_pts(d, frame);
        break;
    }
    case NoStream:
        assert(false);
    }
    
//     if (d->packet.pts == AV_NOPTS_VALUE)
//         std::cout << "Packet PTS is: unknown | ";
//     else
//         std::cout << "Packet PTS is: " << d->packet.pts << " | ";
//     
//     if (d->frame->pts == AV_NOPTS_VALUE)
//         std::cout << "Frame PTS is: unknown" << std::endl;
//     else
//         std::cout << "Frame PTS is: " << d->frame->pts << std::endl;
    
    d->frame_nr++;
}

bool FFMPEGDecoder::readPacket() {
    d->packetType = NoFrameType;
    do {
        av_init_packet(&d->packet);
        if ( av_read_frame(d->formatContext, &d->packet) < 0 ) {
            av_free_packet( &d->packet );
            d->packetSize = 0;
            d->packetData = 0;
            return false;
        }
        if (d->packet.stream_index == d->audioStream) {
            d->packetType = AudioFrameType;
            d->packetSize = d->packet.size;
            d->packetData = d->packet.data;
            return true;
        } else
        if (d->packet.stream_index == d->videoStream) {
            d->packetType = VideoFrameType;
            d->packetSize = d->packet.size;
            d->packetData = d->packet.data;
            return true;
        }
        av_free_packet(&d->packet);
    } while (true);

    return false;
}

bool FFMPEGDecoder::decodePacket() {
    d->nextType = NoFrameType;
    if (!readPacket()) {
        std::cerr << "avkode: FFMPEG: EOF guessed\n";
        d->eof = true;
        return false;
    }
    if (d->packetType == AudioFrameType) {
        d->retries = 0;
        do {
            int gotFrame = 0;
//             avcodec_get_frame_defaults(d->frame);
            int len = avcodec_decode_audio4( d->formatContext->streams[d->audioStream]->codec,
                                             d->frame, &gotFrame, &d->packet);
            if (len <= 0 || gotFrame == 0) {
                d->retries++;
                if (d->retries > 8) {
                    std::cerr << "avkode: FFMPEG: Decoding failure\n";
                    d->error = true;
                    return false;
                }
                continue;
            } else
                d->retries = 0;
            // assume len == d->packetSize
            switch (d->audio_config.sample_width) {
                case 8:
                    demux<int8_t>(d,&d->audioFrame);
                    break;
                case 16:
                    demux<int16_t>(d,&d->audioFrame);
                    break;
                case 32:
                    demux<int32_t>(d,&d->audioFrame);
                    break;
                case -32:
                    demux<float>(d,&d->audioFrame);
                    break;
                default:
                    assert(false);
            }
            d->nextType = AudioFrameType;
        } while (false);
    } else
    if (d->packetType == VideoFrameType) {
        d->retries = 0;
        do {
            int len = avcodec_decode_video2( d->formatContext->streams[d->videoStream]->codec,
                                            d->frame, &d->picture,
                                            &d->packet);
            if (len <= 0) {
                d->retries++;
                if (d->retries > 8) {
                    std::cerr << "avkode: FFMPEG: Decoding failure\n";
                    d->error = true;
                    return false;
                }
                continue;
            } else
                d->retries = 0;
            // assume d->picture != 0
            if (d->picture) {
                convert(d, &d->videoFrame);
                d->nextType = VideoFrameType;
            } else {
                std::cout << "avkode: FFMPEG: Partial video-frame\n";
                d->nextType = NoFrameType;
            }
        } while (false);
    }

    av_free_packet( &d->packet );

    d->lastType = d->packetType;
    if (d->nextType == NoFrameType) return decodePacket();

    return true;
}

bool FFMPEGDecoder::decodeFrame()
{
    return decodePacket();
}

bool FFMPEGDecoder::readAudioFrame(AudioFrame* frame)
{
    if (d->nextType != AudioFrameType) return false;

    swapAudioFrames(frame, &d->audioFrame);

    d->nextType = NoFrameType;

    return true;
}

bool FFMPEGDecoder::readVideoFrame(VideoFrame* frame)
{
    if (d->nextType != VideoFrameType) return false;

    /*
    if (d->interlaced == 1) {
        d->reinterlace.writeVideoFrame(&d->videoFrame, d->topFirst);
        if (!d->reinterlace.readVideoFrame(frame))
            assert(false);
        if (!d->reinterlace.readVideoFrame(&d->interlacedFrame))
            assert(false);
        d->interlaced = 2;
        return true;
    } else
    if (d->interlaced == 2) {
        swapVideoFrames(frame, &d->interlacedFrame);
        d->interlaced = 0;
        d->nextType = NoFrameType;
        return true;
    }*/

    swapVideoFrames(frame, &d->videoFrame);

    d->nextType = NoFrameType;

    return true;
}

FrameType FFMPEGDecoder::frameType() {
    return d->nextType;
}

long FFMPEGDecoder::length() {
    if (!d->initialized) return -1;

    return d->length;
}

long FFMPEGDecoder::position() {
    if (!d->initialized || d->audio_config.sample_rate == 0) {
        std::cout << "Not initialized\n";
        return -1;
    }
    // Using position in audio-track
    int div = d->position / d->audio_config.sample_rate;
    int rem = d->position % d->audio_config.sample_rate;
    return div * 1000L + (rem * 1000L) / d->audio_config.sample_rate;
}

bool FFMPEGDecoder::eof() {
    return d->eof;
}

bool FFMPEGDecoder::error() {
    return d->error;
}

bool FFMPEGDecoder::seekable() {
    return d->src->seekable();
}

bool FFMPEGDecoder::seek(long pos) {
    if (!d->initialized) return false;

    int seekStream = -1;
    if (d->videoStream != -1)
        seekStream = d->videoStream;
    else
        seekStream = d->audioStream;

    AVRational time_base = d->formatContext->streams[seekStream]->time_base;
    std::cout<< "time base is " << time_base.num << "/" << time_base.den << "\n";

    AVRational avkode_time;
    avkode_time.num = pos; avkode_time.den = 1000;

    AVRational fftime = av_div_q(avkode_time, time_base);
    std::cout<< "seeking to " << pos << "ms\n";
//     long ffpos = (long)((pos/1000.0)*AV_TIME_BASE);
    long ffpos = (long)av_q2d(fftime);
    std::cout<< "seeking to " << ffpos << "pos\n";
    bool res = av_seek_frame(d->formatContext, seekStream, ffpos, 0);
    if (res < 0) {
        std::cout<< "seeking failed\n";
        return false;
    }
    else {
        long div = (pos / 1000) * d->audio_config.sample_rate;
        long rem = (pos % 1000) * d->audio_config.sample_rate;
        d->position = div + rem / 1000;
        d->frame_pos = pos;
        return true;
    }
}

const AudioConfiguration* FFMPEGDecoder::audioConfiguration() {
    if (!d->initialized) return 0;
    return &d->audio_config;
}

bool FFMPEGDecoder::hasVideo()
{
    return d->videoStream != -1;
}


} // namespace

// #endif // HAVE_FFMPEG
