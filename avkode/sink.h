/*  AvKode: Sinks

    Copyright (C) 2006 Allan Sandfeld Jensen <kde@carewolf.com>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Steet, Fifth Floor,
    Boston, MA 02110-1301, USA.
*/

#ifndef _AVKODE_SINK_H
#define _AVKODE_SINK_H

namespace AvKode {

class AvFrame;
class AudioFrame;
class VideoFrame;
class AudioConfiguration;

class AvSink {
public:
    virtual ~AvSink() {}
    virtual bool writeAvFrame(AvFrame *) = 0;
};

class AudioSink {
public:
    virtual ~AudioSink() {}
    virtual bool open() = 0;
    virtual void close() = 0;
//     virtual int setAudioConfiguration(const AudioConfiguration *config) = 0;
//     virtual const AudioConfiguration* audioConfiguration() const = 0;
    virtual bool writeAudioFrame(AudioFrame*) = 0;
    virtual int latency() { return 0; }
};

class VideoSink {
public:
    virtual ~VideoSink() {}
    virtual bool open() = 0;
    virtual void close() = 0;
    virtual bool writeVideoFrame(VideoFrame*) = 0;
    virtual int latency() { return 0; }
};


} // namespace


#endif
