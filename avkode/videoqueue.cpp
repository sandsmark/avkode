/*  AvKode: Video Queue

    Copyright (C) 2006 Allan Sandfeld Jensen <kde@carewolf.com>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Steet, Fifth Floor,
    Boston, MA 02110-1301, USA.
*/

#include "videoqueue.h"
#include "avpool.h"
#include "avtime.h"
#include "thread.h"
#include "sink.h"
#include "avframe.h"

#include <assert.h>
#include <queue>
#include <iostream>

using std::queue;

namespace AvKode {

struct VideoQueue::private_data {
    private_data() : sink(0), halt(false) {}

    VideoSink *sink;
    Mutex mutex;
    Condition not_empty;

    AvFramePool pool;

    // Replace with specizialed threadsafe queues
    queue<Time> time_queue;
    queue<VideoFrame*> frame_queue;

    volatile bool halt;
    pthread_t video_thread;
};

void handleFrame(VideoQueue::private_data *d) {
        d->mutex.lock();

        if (d->time_queue.empty())
            d->not_empty.wait(&d->mutex);
        if (d->halt) return;

        assert(!d->time_queue.empty());
        assert(!d->frame_queue.empty());

        Time time = d->time_queue.front();
        d->time_queue.pop();
        VideoFrame *frame = d->frame_queue.front();
        d->frame_queue.pop();

        d->mutex.unlock();

        Time current_time = getTime();
        if (time > current_time) {
            int wait_time = time - current_time;
            AvKode::sleep(wait_time);
        }

        if (time + 50 > current_time)
            d->sink->writeVideoFrame(frame);
        else
            // skip the frame if more than 50ms behind
            std::cout << "VQueue: behind: " << current_time - time << "ms\n";

        d->mutex.lock();
        d->pool.putVideoFrame(frame);
        d->mutex.unlock();
}

void* run(void *opaque) {
    VideoQueue::private_data *d = (VideoQueue::private_data*)opaque;

    while (!d->halt) {
        handleFrame(d);
    }
    return 0;
}

VideoQueue::VideoQueue() {
    d = new private_data;
}

VideoQueue::~VideoQueue() {
    if (d->sink) close();
    delete d;
}

void VideoQueue::open(VideoSink *sink) {
    d->sink = sink;

    pthread_create(&d->video_thread, 0, run, d);
}

void VideoQueue::close() {
    d->halt = true;
    d->not_empty.signal();
    pthread_join(d->video_thread, 0);
    d->sink = 0;

    // free vframes
    while (!d->frame_queue.empty()) {
        delete d->frame_queue.front();
        d->frame_queue.pop();
        d->time_queue.pop();
    }
}

void VideoQueue::delayVideoFrame(int delay, VideoFrame *frame)
{

//     assert (delay < 40000);

    d->mutex.lock();

    // We can't take over frame, so we take over its content
    VideoFrame *pframe = d->pool.getVideoFrame();
    swapVideoFrames(pframe, frame);

    Time current_time = getTime();
    Time time = current_time + delay;
    d->time_queue.push(time);
    d->frame_queue.push(pframe);

    if (d->time_queue.size() > 16) {
        // check for stalled video output
        while (!d->time_queue.empty() &&  d->time_queue.front() + 50 < current_time) {
            d->time_queue.pop();
            d->frame_queue.pop();
            std::cout << "VQueue: stalled\n";
        }
    }

    if (!d->time_queue.empty())
        d->not_empty.signal();

    d->mutex.unlock();
}


} // namespace
