/*  AvKode: Time stuff

    Copyright (C) 2006 Allan Sandfeld Jensen <kde@carewolf.com>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Steet, Fifth Floor,
    Boston, MA 02110-1301, USA.
*/

#ifndef AVKODE_TIME_H
#define AVKODE_TIME_H

#include <unistd.h>
#include <sys/time.h>
#include <sys/poll.h>
#include <time.h>

#include <stdint.h>

namespace AvKode {
    typedef int64_t Time;

    inline Time getTime() {
        timeval tv;
        ::gettimeofday(&tv, 0);
        Time st = tv.tv_sec;
        Time ut = tv.tv_usec;
        st = st * 1000L;
        ut = ut / 1000L;
        return st + ut;
    }

    inline void sleep(unsigned int msecs) {
        ::poll(0,0,msecs);
    }
}

#endif
