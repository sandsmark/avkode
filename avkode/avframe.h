/*  AvKode AvFrame

    Copyright (C) 2004,2006 Allan Sandfeld Jensen <kde@carewolf.com>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Steet, Fifth Floor,
    Boston, MA 02110-1301, USA.
*/

#ifndef _AKODE_AVFRAME_H
#define _AKODE_AVFRAME_H

#include <stdint.h>

#include "avconfiguration.h"
#include "avtime.h"

#include <assert.h>

namespace AvKode {

#define AKODE_POS_UNKNOWN -1

enum FrameType {
    NoFrameType,
    AudioFrameType,
    VideoFrameType,
    DataFrameType
};

struct AvFrame {
    AvFrame(FrameType _type) : type(_type), pos(-1) {};
    virtual ~AvFrame() {};
    /*!
     * Type of frame
     */
    FrameType type;
    /*!
     * The current position in stream (measured in milliseconds)
     * -1 if unknown
     */
    long pos;

private:
    AvFrame(const AvFrame&) {assert(false);}
};


//! The internal audio format

/*!
 * AudioFrames are used through-out avkode as the mean of audiotransport.
 * It derives from AudioConfiguration because it caries its own interpretation
 * around with it.
 */
struct AudioFrame : public AvFrame, public AudioConfiguration {
public:
    AudioFrame() : AvFrame(AudioFrameType), length(0), max(0), data(0) {};
    ~AudioFrame() { freeSpace(); }
    /*!
     * Reserves space in the frame for atleast \a iLength samples of the
     * configuration \a config.
     */
    void reserveSpace(const AudioConfiguration *config, long iLength) {
        reserveSpace(config->channels, iLength, config->sample_width);
        sample_rate = config->sample_rate;
        channel_config = config->channel_config;
        surround_config = config->surround_config;
    }
    void reserveSpace(uint8_t iChannels, long iLength, int8_t iWidth) {
        // Sanity
        assert(iChannels > 0);
        assert(iWidth != 0 && iWidth >= -64 && iWidth <= 32);

        // No reallocation needed
        if ( data != 0 && channels == iChannels &&
             max >= iLength && sample_width == iWidth)
        {
            length = iLength;
            return;
        }

	// Reallocate
	freeSpace();
	channels = iChannels;
	length = max = iLength;
	sample_width = iWidth;
	if (length == 0) {
	    data = 0;
	    return;
        }
	data = new int8_t*[channels+1];
	int bytes = 0;
	if (sample_width < 0) {
	   if (sample_width == -32)
	       bytes = 4;
	   else
	   if (sample_width == -64)
	       bytes = 8;
	   else
               assert(false);
        }
	else {
	   bytes = (sample_width+7) / 8;
           if (bytes == 3) bytes = 4; // 24bit uses 4 bytes
        }

	for(int i=0; i<iChannels; i++)
	    data[i] = new int8_t[length*bytes];
	data[iChannels] = 0;
    }
    /*!
     * Frees the space allocated for the buffer.
     */
    void freeSpace()
    {
        if (!data) return;
	int8_t** tmp = data;
	while(*tmp) {
	   delete[] *tmp;
	   tmp++;
	}
	delete[] data;
        pos = 0;
	data = 0;
	channels = 0;
	length = 0;
        max = 0;
    }
    bool isNull() const {
        return (data == 0);
    }
    bool isEmpty() const {
        return (length == 0);
    }
    int duration() const {
        if (sample_rate == 0) return -1;
        int div = length / sample_rate;
        int rem = length % sample_rate;
        return div * 1000 + (rem * 1000)/sample_rate;
    }
    /*!
     * The length of the frame in samples.
     */
    long length;
    /*!
     * The maximum number of samples currently reserved in the frame.
     */
    long max;
    /*!
     * The buffer is accessed according sample-width
     * 1-8bit: int8_t, 9-16bit: int16_t, 17-32bit: int32_t
     * -32bit: float, -64bit: double
     */
    int8_t** data;
};

struct VideoFrame : public AvFrame, public VideoConfiguration {
public:
    VideoFrame() : AvFrame(VideoFrameType), max(0), data(0) { width = 0; height = 0; }
    ~VideoFrame() { freeSpace(); }
    void reserveSpace(VideoConfiguration config) {
        reserveSpace((ColorFormat)config.color_format, config.width, config.height);
        aspect_ratio = config.aspect_ratio;
        interlace_type = config.interlace_type;
    }
    void reserveSpace(ColorFormat _color_format, int _width, int _height) {
        width = _width;
        height = _height;
        color_format = _color_format;
        int bytes = 0;
        int sample = 1;
        switch (color_format) {
        case RGB32:
            // 4 bytes per pixel
            bytes = 4;
            sample = 1;
            break;
        case YUY2:
            // 4 bytes per 2 pixels
            bytes = 4;
            sample = 2;
            break;
        case YV12:
            // 6 bytes per 4 pixels
            bytes = 6;
            sample = 4;
            break;
        default:
            assert(false);
        }
        unsigned int samples = (width * height + (sample-1))/sample;
        // align to sizeof(void*) to allow for fast memory access
        int align = sizeof(void*) - 1;
        unsigned int size = (samples * bytes + align) & ~align;
        if (size < max) return;
        freeSpace();
        data = new uint8_t[size];
        max = size;
    }
    void freeSpace() {
        delete[] data;
        data = 0;
        max = 0;
    }
    bool isNull() const {
        return data == 0;
    }
    bool isEmpty() const {
        return (width * height) == 0;
    }
    /*!
     * The duration of the frame in milliseconds.
     */
    long duration;
    /*!
     * Maximal size of the data-buffer
     */
    unsigned int max;
    /*!
     * Synchronization TimeStamp
     */
    Time sts;
    /*!
     * The data in RGB32
     */
    uint8_t *data;
};

// evil function to swap the contents of two frames
inline void swapAudioFrames(AudioFrame* toFrame, AudioFrame* fromFrame) {
    AudioFrame tmpFrame;

    tmpFrame = *toFrame;
    *toFrame = *fromFrame;
    *fromFrame = tmpFrame;
    tmpFrame.data = 0;
}

inline void swapVideoFrames(VideoFrame* toFrame, VideoFrame* fromFrame) {
    VideoFrame tmpFrame;

    tmpFrame = *toFrame;
    *toFrame = *fromFrame;
    *fromFrame = tmpFrame;
    tmpFrame.data = 0;
}

} // namespace

#endif
