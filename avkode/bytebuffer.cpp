/*  AvKode: ByteBuffer

    Copyright (C) 2004 Allan Sandfeld Jensen <kde@carewolf.com>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Steet, Fifth Floor,
    Boston, MA 02110-1301, USA.
*/


#include "bytebuffer.h"
#include "thread.h"

#include <string.h>

namespace AvKode {

ByteBuffer::ByteBuffer(unsigned int len)
              : length(len)
              , readPos(0)
              , writePos(0)
              , flushed(false)
              , released(false)
              , open(false)
              , closed(false)
{
    buffer = new char[length];
}

ByteBuffer::~ByteBuffer() {
    delete[] buffer;
}

void ByteBuffer::setBufferSize(unsigned int len) {
    mutex.lock();
    unsigned int clen = content();
    char* newbuffer = new char[len];

    // preserve the last of the buffer if we have to discard something
    if (clen > len) {
        readPos = readPos + (clen-len);
        if (readPos > length) readPos = readPos - length;
        clen = len;
    }

    unsigned int base = clen, rem = 0;
    if (readPos+clen > length) {
        base = length-readPos;
        rem = clen-base;
    }

    memcpy(newbuffer, buffer+readPos, base);
    memcpy(newbuffer+base, buffer, rem);

    delete[] buffer;
    buffer = newbuffer;
    length = len;

    mutex.unlock();
}

unsigned int ByteBuffer::bufferSize() const {
    return length;
}

int ByteBuffer::write(char* buf, unsigned int len, bool blocking)
{
    mutex.lock();
    if (released) len = 0;
    if (closed) len = 0;

    if (!open && len > 0) {
        open = true;
        not_closed.signal();
    }

    flushed = closed = false;
    while (space() < len) {
        if (blocking) {
            not_full.wait(&mutex);
            if (flushed || released) len = 0;
        }
        else
            len = space();
    }

    unsigned int base = len, rem = 0;
    if (writePos+len > length) {
        base = length-writePos;
        rem = len-base;
    }

    memcpy(buffer+writePos, buf, base);
    memcpy(buffer, buf+base, rem);

    writePos = (writePos+len) % length;

    not_empty.signal();
    mutex.unlock();
    return len;
}

int ByteBuffer::read(char* buf, unsigned int len, bool blocking)
{
    mutex.lock();
    if (released) len = 0;
    if (closed) blocking = false;

    if (!open && !closed) {
        if (blocking)
            not_closed.wait(&mutex);
        else
            len = 0;
    }

    while (content() < len) {
        if (blocking) {
            not_empty.wait(&mutex);
            if (released)
                len = 0;
            else if (closed)
                len = content();
        }
        else
            len = content();
    }

    unsigned int base = len, rem = 0;
    if (readPos+len > length) {
        base = length-readPos;
        rem = len-base;
    }

    memcpy(buf, buffer+readPos, base);
    memcpy(buf+base, buffer, rem);

    readPos = (readPos+len) % length;

    not_full.signal();
    mutex.unlock();
    return len;
}

int ByteBuffer::waitFor(unsigned int len)
{
    mutex.lock();

    if (!open && !released && !closed)
        not_closed.wait(&mutex);

    if (released) len = 0;
    else
    if (closed) len = content();

    // Cannot wait for more than buffer-size
    if (len > length) len = length;

    while (content() < len) {
        not_empty.wait(&mutex);
        if (released)
            len = 0;
        else if (closed)
            len = content();
    }

    mutex.unlock();
    return len;
}

void ByteBuffer::start() {
    mutex.lock();
    closed = false;
    open = true;
    not_closed.signal();
    mutex.unlock();
}

void ByteBuffer::end() {
    mutex.lock();
    closed = true;
    not_empty.signal();
    mutex.unlock();
}

bool ByteBuffer::eof() const {
    return empty() && closed;
}

bool ByteBuffer::empty() const {
    return (readPos == writePos);
}

bool ByteBuffer::started() const {
    return open;
}

bool ByteBuffer::full() const {
    return (readPos == (writePos+1) % length);
}

unsigned int ByteBuffer::content() const {
    unsigned int cn;
    if (readPos <= writePos)
        cn=writePos-readPos;
    else
        cn=writePos+length-readPos;
    return cn;
}

unsigned int ByteBuffer::space() const {
    unsigned int sp = length - content() - 1;
    return sp;
}

void ByteBuffer::reset() {
    // We assume all processes have been released at this point
    readPos = writePos = 0;
    flushed = released = closed = open = false;
}

void ByteBuffer::flush() {
    mutex.lock();
    readPos = writePos = 0;
    flushed = true;
    not_full.signal();
    mutex.unlock();
}

void ByteBuffer::release() {
    mutex.lock();
    released = true;
    not_empty.signal();
    not_full.signal();
    not_closed.signal();
    mutex.unlock();
}

bool ByteBuffer::error() const {
    return released || (closed && !open);
}

} // namespace

