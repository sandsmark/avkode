/*  AvKode: ALSA Sink

    Copyright (C) 2004-2005 Allan Sandfeld Jensen <kde@carewolf.com>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Steet, Fifth Floor,
    Boston, MA 02110-1301, USA.
*/

#include <iostream>

#include <alsa/asoundlib.h>
#include <alsa/pcm.h>

#include <avframe.h>
#include "alsa_sink.h"

/*
 * resume from suspend
 */
static int resume(snd_pcm_t *pcm)
{
  int res;
  while ((res = snd_pcm_resume(pcm)) == -EAGAIN)
    sleep(1);
  if (! res)
    return 0;
  return snd_pcm_prepare(pcm);
}

namespace AvKode {

struct ALSASink::private_data
{
    private_data() : pcm_playback(0), buffer(0), error(false), can_pause(false), initialized(false) {};

    snd_pcm_t *pcm_playback;

    AudioConfiguration config;
    int scale;
    int filled, fragmentSize;
    int sampleSize;
    char* buffer;
    bool error;
    bool can_pause;
    bool initialized;
};

ALSASink::ALSASink()
{
    d = new private_data;
}

ALSASink::~ALSASink()
{
    close();
    delete d;
}

bool ALSASink::open()
{
    return open("default");
}

bool ALSASink::open(const char* device)
{
    int err = 0;
    // open is non-blocking to make it possible to fail when occupied
    err = snd_pcm_open(&d->pcm_playback, device, SND_PCM_STREAM_PLAYBACK, SND_PCM_NONBLOCK);
    if (err < 0) {
        d->error = true;
        return false;
    }

    // Set to blocking
    snd_pcm_nonblock(d->pcm_playback, 0);

    d->error = false;
    return true;
}

void ALSASink::close()
{
    if (d->pcm_playback) {
        snd_pcm_drain(d->pcm_playback);
        snd_pcm_close(d->pcm_playback);
    }
    d->pcm_playback = 0;
    d->error = false;
    d->initialized = false;
}

int ALSASink::setAudioConfiguration(const AudioConfiguration* config)
{
    if (d->error) return -1;

    if (!d->pcm_playback && !open())
            return -1;
        
    // Get back to OPEN state (is SETUP state enough with snd_pcm_drop?)
    snd_pcm_state_t state = snd_pcm_state( d->pcm_playback );
    if (state != SND_PCM_STATE_OPEN) {
        close();
        if (!open()) return -1;
    }

    int res = 0;
    int wid = 1;
    d->config = *config;
    snd_pcm_hw_params_t *hw;
    snd_pcm_hw_params_alloca(&hw);
    snd_pcm_hw_params_any(d->pcm_playback, hw);
    // ### use non-interleaved when available
    snd_pcm_hw_params_set_access(d->pcm_playback, hw, SND_PCM_ACCESS_RW_INTERLEAVED);
    // Detect format:
    snd_pcm_format_t format = SND_PCM_FORMAT_UNKNOWN;
    // Test for float, 24 and 32 bit integer. Fall back to 16bit
    if (d->config.sample_width<0) {
        if (snd_pcm_hw_params_test_format(d->pcm_playback, hw,  SND_PCM_FORMAT_FLOAT ) == 0) {
            format = SND_PCM_FORMAT_FLOAT;
            d->scale = 1;
            wid = 4;
            goto found_format;
        }
        // Try 16bit then
        d->config.sample_width = 16;
        res = 1;
    }
    if (d->config.sample_width > 24 && d->config.sample_width <=32) {
        if (snd_pcm_hw_params_test_format(d->pcm_playback, hw, SND_PCM_FORMAT_S32) == 0) {
            format = SND_PCM_FORMAT_S32;
            d->scale = 1<<(32-config->sample_width);
            wid = 4;
            goto found_format;
        }
        // Try 24bit then
        d->config.sample_width = 24;
        res = 1;
    }
    if (d->config.sample_width > 16 && d->config.sample_width <= 24) {
        if (snd_pcm_hw_params_test_format(d->pcm_playback, hw,  SND_PCM_FORMAT_S24 ) == 0) {
            format = SND_PCM_FORMAT_S24;
            d->scale = 1<<(24-config->sample_width);
            wid = 4;
            goto found_format;
        }
        // Try 16bit then
        d->config.sample_width = 16;
    }
    // If the driver doesnt support 8 or 16 bit, we will fail completly
    if (d->config.sample_width<=8) {
        format = SND_PCM_FORMAT_S8;
        d->scale = 1<<(8-config->sample_width);
        wid = 1;
        goto found_format;
    }
    else
    if (d->config.sample_width<=16) {
        format = SND_PCM_FORMAT_S16;
        d->scale = 1<<(16-config->sample_width);
        wid = 2;
        goto found_format;
    }

found_format:
    if (format != SND_PCM_FORMAT_UNKNOWN)
        snd_pcm_hw_params_set_format(d->pcm_playback, hw, format);
    else
        return -1;

    unsigned int rate = config->sample_rate;
    snd_pcm_hw_params_set_rate_near(d->pcm_playback, hw, &rate, 0);
    if (d->config.sample_rate != rate) {
        d->config.sample_rate = rate;
        res = 1;
    }

    snd_pcm_hw_params_set_channels(d->pcm_playback, hw, config->channels);


    d->fragmentSize = 1024;
    snd_pcm_uframes_t period_size = d->fragmentSize / (wid*config->channels);
    snd_pcm_hw_params_set_period_size_near(d->pcm_playback, hw, &period_size, 0);

    d->fragmentSize = period_size * (wid*config->channels);
//     std::cerr << "akode: ALSA fragment-size: " << d->fragmentSize << "\n";

    delete [] d->buffer;
    d->buffer = new char [d->fragmentSize];
    d->filled = 0;

    int err = snd_pcm_set_params(d->pcm_playback, format, SND_PCM_ACCESS_RW_INTERLEAVED, config->channels, config->sample_rate, 1, 0);
    //if (snd_pcm_hw_params(d->pcm_playback, hw) < 0) {
    if (err) {
        std::cerr << "unable to set hw params\n";
        d->initialized = false;
        return -1;
    }
    else {
        d->can_pause = (snd_pcm_hw_params_can_pause(hw) == 1);
        d->initialized = true;
        return res;
    }
}

const AudioConfiguration* ALSASink::audioConfiguration() const
{
    return &d->config;
}

template<class T>
bool ALSASink::_writeFrame(AudioFrame* frame)
{
    int channels = d->config.channels;

    long i = 0;
    T* buffer = (T*)d->buffer;
    T** data = (T**)frame->data;
    while(true) {
        if (d->filled >= d->fragmentSize)
        xrun:
        {
           snd_pcm_sframes_t frames = snd_pcm_bytes_to_frames(d->pcm_playback, d->filled);
           int status = snd_pcm_writei(d->pcm_playback, d->buffer, frames);
//              int status = snd_pcm_writei(d->pcm_playback, d->buffer, d->fragmentSize);

            if (status == -EPIPE) {
                snd_pcm_prepare(d->pcm_playback);
                //std::cerr << "akode: ALSA xrun\n";
                goto xrun;
            }
            else if (status < 0) {
                std::cerr << "akode: alsa_sink error: " << snd_strerror(status) << std::endl;
                return false;
            }
            int bytes = snd_pcm_frames_to_bytes(d->pcm_playback, status);
            if (d->filled != bytes) {
                int rest = d->filled - bytes;
                std::cout << "akode: ALSA write-remainder: " << rest << "\n";
                memmove(d->buffer, d->buffer + bytes, rest);
                d->filled = rest;
            } else
                d->filled = 0;

        }
        if (i >= frame->length) break;
        for(int j=0; j<channels; j++) {
            buffer[d->filled/sizeof(T)] = (data[j][i])*d->scale;
            d->filled+=sizeof(T);
        }
        i++;
    }

    if (snd_pcm_state( d->pcm_playback ) == SND_PCM_STATE_PREPARED)
        snd_pcm_start(d->pcm_playback);

    return true;
}

bool ALSASink::writeAudioFrame(AudioFrame* frame)
{
    if (d->error) return false;
    if (!frame) return false;

    if ( !d->initialized
      || frame->sample_width != d->config.sample_width
      || frame->channels != d->config.channels
      || frame->sample_rate != d->config.sample_rate)
    {
        if (setAudioConfiguration(frame) < 0)
            return false;
    }

    if ( snd_pcm_state(d->pcm_playback) == SND_PCM_STATE_SUSPENDED ) {
      int res = ::resume(d->pcm_playback);
      if (res < 0)
        return false;
    }
    else
    if (snd_pcm_state( d->pcm_playback ) == SND_PCM_STATE_PAUSED)
        snd_pcm_pause(d->pcm_playback, 0);

    if (snd_pcm_state( d->pcm_playback ) == SND_PCM_STATE_SETUP)
        snd_pcm_prepare(d->pcm_playback);

    if (frame->sample_width<0)
        return _writeFrame<float>(frame);
    else
    if (frame->sample_width<=8)
        return _writeFrame<int8_t>(frame);
    else
    if (frame->sample_width<=16)
        return _writeFrame<int16_t>(frame);
    else
    if (frame->sample_width<=32)
        return _writeFrame<int32_t>(frame);

    return false;
}

void ALSASink::pause()
{
    if (d->error) return;

    if (d->can_pause) {
        snd_pcm_pause(d->pcm_playback, 1);
    }

}

// Do not confuse this with snd_pcm_resume which is used to resume from a suspend
void ALSASink::resume()
{
    if (d->error) return;

    if (snd_pcm_state( d->pcm_playback ) == SND_PCM_STATE_PAUSED)
        snd_pcm_pause(d->pcm_playback, 0);
}

int ALSASink::latency()
{
    if (d->error || !d->initialized || d->config.sample_rate == 0) return 0;

    snd_pcm_sframes_t frames;

    snd_pcm_delay(d->pcm_playback, &frames);

    if (snd_pcm_state( d->pcm_playback ) != SND_PCM_STATE_RUNNING)
        return 0;

    // delay in ms after normal rounding
    int sample_rate = d->config.sample_rate;
    long div = (frames / sample_rate) * 1000;
    long rem = (frames % sample_rate) * 1000;

    return div + rem / sample_rate;;
}

} // namespace
