/*  AvKode: FFMPEG Decoder

    Copyright (C) 2005 Allan Sandfeld Jensen <kde@carewolf.com>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Steet, Fifth Floor,
    Boston, MA 02110-1301, USA.
*/

#ifndef _AKODE_FFMPEG_DECODER_H
#define _AKODE_FFMPEG_DECODER_H

#include <kdemacros.h>
// #ifdef HAVE_FFMPEG

namespace AvKode {

class ByteStream;
class AudioFrame;
class AvFrame;

class KDE_EXPORT FFMPEGDecoder {
public:
    FFMPEGDecoder();
    virtual ~FFMPEGDecoder();

    virtual bool open(ByteStream* src);
    void close();

    virtual bool decodeFrame();
    virtual FrameType frameType();
    virtual bool readAudioFrame(AudioFrame*);
    virtual bool readVideoFrame(VideoFrame*);
    virtual long length();
    virtual long position();
    virtual bool seek(long);
    virtual bool seekable();
    virtual bool eof();
    virtual bool error();
    virtual bool hasVideo();

    virtual const AudioConfiguration* audioConfiguration();

    struct private_data;
private:
    bool readPacket();
    bool decodePacket();
    private_data *d;
};

} // namespace

// #endif // HAVE_FFMPEG
#endif
