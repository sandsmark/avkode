/*  This file is part of the KDE project
    Copyright (C) 2006 Allan Sandfeld Jensen (kde@carewolf.com)

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301, USA.

*/
#ifndef KIOSTREAM_H
#define KIOSTREAM_H

#include <QtCore/QObject>
#include <kio/jobclasses.h>
#include <kio/job.h>

#include <QtCore/QByteArray>
#include <stdint.h>

namespace AvKode {
    class ByteBuffer;
}

// Serves as a stream abstraction atop of KIO::Open
class KIOStream : public QObject
{
Q_OBJECT
public:
    KIOStream(AvKode::ByteBuffer *buffer);
    ~KIOStream();

    bool openStream(const KUrl& url);
    void closeStream();

    void resume();
    void suspend();
    uint64_t length();
    uint64_t position();
    void seek(uint64_t pos);
    void requestData(int size);

    const char* filename() const;

private slots:
    void slotData(KIO::Job *job, const QByteArray &data);
    void slotPosition(KIO::Job *job, KIO::filesize_t offset);
    void slotOpen(KIO::Job *job);
    void slotClose(KIO::Job *job);

    void slotResult(KJob *job);

private:
    struct private_data;
    private_data *d;
};

#endif
