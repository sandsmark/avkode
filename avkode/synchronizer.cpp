/*  AvKode: Synchronizer

    Copyright (C) 2006 Allan Sandfeld Jensen <kde@carewolf.com>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Steet, Fifth Floor,
    Boston, MA 02110-1301, USA.
*/


#include "synchronizer.h"

#include "avframe.h"
#include "avtime.h"
#include "sink.h"

#include "audioqueue.h"
#include "videoqueue.h"

#include <iostream>

namespace AvKode {

struct Synchronizer::private_data {
    private_data() : asink(0), vsink(0),
                     last_apos(-1), last_vpos(-1),
                     last_atime(0), last_vtime(0),
                     last_type(NoFrameType)
    {}

    AudioSink *asink;
    VideoSink *vsink;

    AudioQueue aqueue;
    VideoQueue vqueue;

    long last_apos;
    long last_vpos;
    Time last_atime;
    Time last_vtime;
    FrameType last_type;
};

Synchronizer::Synchronizer() {
    d = new private_data;
}

Synchronizer::~Synchronizer() {
    delete d;
}

void Synchronizer::setVideoSink(VideoSink *vsink)
{
    d->vsink = vsink;
    d->vqueue.open(vsink);
//     d->last_time = getTime();
}

void Synchronizer::setAudioSink(AudioSink *asink)
{
    d->asink = asink;
    d->aqueue.open(asink);
}

void Synchronizer::writeAvFrame(AvFrame *frame)
{
    assert (d->asink);
    assert (d->vsink);

    FrameType current_type = frame->type;
    Time current_time = getTime();
    switch (current_type) {
        case AudioFrameType: {
            AudioFrame *aframe = static_cast<AudioFrame*>(frame);
//             std::cout << "AFrame\n";
            int lat = d->asink->latency();
            if (lat < 0 || lat > 1000)
                std::cout << "Fucked asink latency: " << lat << std::endl;
            d->last_atime = current_time + d->aqueue.minimumDelay() + lat;
            d->last_apos = frame->pos;
            d->aqueue.writeAudioFrame(aframe);
            break;
        }
        case VideoFrameType: {
//             std::cout << "VFrame\n";
            VideoFrame *vframe = static_cast<VideoFrame*>(frame);
            if (vframe->pos <= d->last_vpos) {
                std::cout << "Non progressive VFrame " << vframe->pos << "<=" << d->last_vpos << "\n";
                break;
            }
            int delay = 0;
            switch (d->last_type) {
                case AudioFrameType: {
                    // match last_atime
                    if (d->last_atime > 0)
                        delay = (d->last_atime - current_time) - (d->last_apos - vframe->pos);
                        // delay = (vframe->pos - d->last_apos)  - (current_time - d->last_atime);
                    break;
                }
                case VideoFrameType: {
                    if (d->last_vtime > 0) {
                        delay = (vframe->pos - d->last_vpos) - (current_time - d->last_vtime);
//                         std::cout << "Time between video-frames: " << delay << "ms\n";
                    }
                    break;
                }
                default:
                    break;
            }
//             std::cout << "to last_atime " << current_time - d->last_atime << std::endl;
//             std::cout << "to last_vtime " << current_time - d->last_vtime << std::endl;
            int lat = d->vsink->latency();
            if (lat < 0 || lat > 1000)
                std::cout << "Fucked vsink latency: " << lat << std::endl;
            delay = delay - lat;
//             delay += d->delay;
            // Increase audio delay
            if (delay < 100) {
                d->aqueue.setMinimumDelay(d->aqueue.minimumDelay() + (100 -delay));
            }
            assert (delay < 50000);
            if (delay > 5000) {
                std::cout << "Fucked up delay: " << delay << "ms\n";
            }
            d->last_vtime = current_time + delay + lat;
            d->last_vpos = vframe->pos;
            vframe->sts = current_time + delay;
            if (delay > 0) {
                d->vqueue.delayVideoFrame(delay, vframe);
            } else {
                std::cout << "Negative video delay: " << delay << "ms\n";
            }
            break;
        }
        default:
            break;
    }
    d->last_type = current_type;
}

void Synchronizer::seeked()
{
   d->last_apos = -1;
   d->last_vpos = -1;
   d->last_atime = 0;
   d->last_vtime = 0;
}

void Synchronizer::pause()
{

}

void Synchronizer::resume()
{

}


} // namespace
