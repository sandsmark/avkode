/*  AvKode: ByteStream

    Copyright (C) 2006 Allan Sandfeld Jensen <kde@carewolf.com>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Steet, Fifth Floor,
    Boston, MA 02110-1301, USA.
*/

#ifndef _AVKODE_BYTESTREAM_H
#define _AVKODE_BYTESTREAM_H

extern "C" {
#include <sys/types.h>
#include <unistd.h>
}

namespace AvKode {

//! A generic interface for all types of byte streams

/*!
 * ByteStream serves as the interface for both files and buffers.
 * It's only thread safe with one reading thread.
 */

class ByteStream {
public:
    virtual ~ByteStream() {}
    /*!
     * Read upto \a len bytes non-blocking.
     * Return the number of bytes read.
     */
    virtual long read(char* buf, unsigned long len) = 0;
    virtual bool seek(long, int = SEEK_SET) { return false; }

    /*!
    * Returns the number of bytes that can be read non-blocking
    */
    virtual long available() const { return 0; }
    /*!
    * Block until \a len bytes is available.
    * Returns the number of bytes available. This number can be smaller than \a len, if
    * the buffer is not large enough for \a len bytes or if the source has reaced end-of-stream.
    * If only one thread is reading this garuantees a read of this many bytes will succede.
    */
    virtual long waitFor(unsigned long len) = 0;
    virtual void setBufferSize(unsigned long /*len*/) {}
    virtual unsigned long bufferSize() const { return -1;}

    virtual long position() const = 0;
    virtual long length() const { return -1; }

    virtual bool seekable() const { return false; }
    virtual const char* filename() const { return 0; }

    virtual void release() = 0;
    virtual void reset() = 0;

    virtual bool eof() const = 0;
    virtual bool error() const = 0;
};


} // namespace

#endif
