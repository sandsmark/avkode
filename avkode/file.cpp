/*  AvKode: File bytestream

    Copyright (C) 2006 Allan Sandfeld Jensen <kde@carewolf.com>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Steet, Fifth Floor,
    Boston, MA 02110-1301, USA.
*/

#include "file.h"
#include "localfile.h"
#include "bytestream.h"

namespace AvKode {

namespace File {
    class ByteStream;
}

class File::ByteStream : public AvKode::ByteStream
{
public:
    ByteStream(FileHandle *file) : file(file) {};
    ~ByteStream() {
        file->close();
    }
    long read(char* buf, unsigned long len) {
        return file->read(buf, len);
    }
    bool seek(long pos, int whence) {
        return file->seek(pos, whence);
    }
    long available() const {
        return file->length() - file->position();
    }
    long position() const {
        return file->position();
    }
    long length() const {
        return file->length();
    }
    bool seekable() const {
        return file->seekable();
    }
    const char* filename() const {
        return file->filename;
    }
    long waitFor(unsigned long) {
        return available();
    }

    void release() {}
    void reset() {}

    bool eof() const {
        return file->eof();
    }
    bool error() const {
        return file->error();
    }
private:
    FileHandle *file;
};

ByteStream* File::open(const QByteArray &filename) {
    FileHandle *fd = new LocalFile(filename);
    if (!fd->openRO()) {
        delete fd;
        return 0;
    }
    fd->fadvise();
    ByteStream *stream = new File::ByteStream(fd);

    return stream;
}

} // namespace
