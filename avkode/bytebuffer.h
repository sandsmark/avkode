/*  AvKode: ByteBuffer

    Copyright (C) 2004,2006 Allan Sandfeld Jensen <kde@carewolf.com>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Steet, Fifth Floor,
    Boston, MA 02110-1301, USA.
*/

#ifndef _AKODE_BYTEBUFFER_H
#define _AKODE_BYTEBUFFER_H

#include "thread.h"

namespace AvKode {

//! A reentrant circular buffer of bytes

/*!
 * A buffer of bytes to synchronize I/O between two threads, one reading and one writing.
 */
class ByteBuffer {
    unsigned int length;
    char* buffer;
    volatile unsigned int readPos;
    volatile unsigned int writePos;
    Mutex mutex;
    Condition not_empty;
    Condition not_full;
    Condition not_closed;
    volatile bool flushed, released, open, closed;
public:
    /*!
     * Constructs a buffer with \a len bytes.
     */
    ByteBuffer(unsigned int len);
    ~ByteBuffer();

    /*!
     * Sets the buffersize to \a len.
     * If the buffer is not empty, this is an expensive operation that will copy
     * all the data.
     * Some content might be losed if set a smaller size.
     */
    void setBufferSize(unsigned int len);

    /*!
     * Returns the current buffer size.
     */
    unsigned int bufferSize() const;

    /*!
     * Write \a len bytes from \a buf into the buffer. If blocking is set to true,
     * write will block until all bytes have been writen or release() or flush()
     * is called.
     * Returns the number of bytes writen.
     */
    int write(char* buf, unsigned int len, bool blocking = false);
    /*!
     * Read \a len bytes from the buffer into \a buf. If blocking is set to true,
     * read will block until \a len bytes have been read or release()
     * is called.
     * Returns the number of bytes read.
     */
    int read(char* buf, unsigned int len, bool blocking = false);
    /*!
     * Waits until there is \a len bytes in the buffer.
     * It might return less if file is smaller, or the block is released.
     * If start() hasn't been called waitFor also waits for start().
     * Returns the number of bytes available.
     */
    int waitFor(unsigned int len);

    /*!
     * Called by the writing thread to indicate start-of-stream
     */
    void start();
    /*!
     * Called by the writing thread to indicate end-of-stream
     */
    void end();

    /*!
     * Returns true if the stream is started - start() has been called
     */
    bool started() const;

    /*!
     * Returns true if the stream is at eof - end() have been called and the buffer is empty
     */
    bool eof() const;

    /*!
     * Returns true if the buffer is empty
     */
    bool empty() const;
    /*!
     * Returns true if the buffer is full
     */
    bool full() const;

    /*!
     * Returns the number of bytes that can be read without blocking
     */
    unsigned int content() const;
    /*!
     * Returns the number of bytes that can be writen without blocking
     */
    unsigned int space() const;

    /*!
     * Flushes the buffer and releases any blocking write-calls.
     */
    void flush() ;
    /*!
     * Releases all blocking threads and prepares the buffer for deletion.
     * Use reset to make the buffer usable again.
     */
    void release();
    /*!
     * Resets the buffer to be as good as new. Assumes all threads are released.
     */
    void reset();
    /*!
     * Return true if the buffer is in unusable state. Usually after release() have been called
     */
    bool error() const;
};

} // namespace

#endif
