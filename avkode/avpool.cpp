/*  AvKode: Frame pool

    Copyright (C) 2006 Allan Sandfeld Jensen <kde@carewolf.com>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Steet, Fifth Floor,
    Boston, MA 02110-1301, USA.
*/

#include "avpool.h"
#include "avframe.h"

#include <stack>
using std::stack;

namespace AvKode {

struct AvFramePool::private_data {
    // uses stack for cache effeciency
    stack<AudioFrame*> apool;
    stack<VideoFrame*> vpool;
};

AvFramePool::AvFramePool() {
    d = new private_data;
}

AvFramePool::~AvFramePool() {
    while (!d->apool.empty()) {
        delete d->apool.top();
        d->apool.pop();
    }
    while (!d->vpool.empty()) {
        delete d->vpool.top();
        d->vpool.pop();
    }
    delete d;
}

AudioFrame* AvFramePool::getAudioFrame()
{
    AudioFrame *new_frame = 0;

    if (d->apool.empty())
        new_frame = new AudioFrame();
    else {
        new_frame = d->apool.top();
        d->apool.pop();
    }

    return new_frame;
}

void AvFramePool::putAudioFrame(AudioFrame *frame)
{
    d->apool.push(frame);
}

VideoFrame* AvFramePool::getVideoFrame()
{
    VideoFrame *new_frame = 0;

    if (d->vpool.empty())
        new_frame = new VideoFrame();
    else {
        new_frame = d->vpool.top();
        d->vpool.pop();
    }

    return new_frame;
}

void AvFramePool::putVideoFrame(VideoFrame *frame)
{
    d->vpool.push(frame);
}

void AvFramePool::putAvFrame(AvFrame *frame)
{
    switch(frame->type) {
    case AudioFrameType:
        putAudioFrame(static_cast<AudioFrame*>(frame));
        break;
    case VideoFrameType:
        putVideoFrame(static_cast<VideoFrame*>(frame));
        break;
    default:
        delete frame;
    }
}

} // namespace
