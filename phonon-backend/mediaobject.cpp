/*  This file is part of the KDE project
    Copyright (C) 2006 Allan Sandfeld Jensen (kde@carewolf.com)
    Copyright (C) 2012 Martin Sandsmark <martin.sandsmark@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301, USA.

*/

#include "mediaobject.h"
#include "audiopath.h"
#include "videopath.h"
#include "videowidget.h"
#include "audiooutput.h"
#include <QTimer>
#include <avkode/bytestream.h>
#include <avkode/kio.h>
#include <avkode/file.h>
#include <avkode/avframe.h>
#include <avkode/ffmpeg_decoder.h>
#include <avkode/synchronizer.h>
#include <iostream>
#include <QtCore/QThread>
#include <QtCore/QSemaphore>
#include <assert.h>

namespace Phonon
{
namespace Avkode
{
class MediaObject::AvkodeThread : public QThread
{
public:
    AvkodeThread(MediaObject *parent) : QThread(parent),
                     m_decoder(0), m_file(0), m_sync(0), m_parent(parent),
                     m_prefinishMark(0), m_timeLeft(0),
                     m_pauseSemaphore(1), m_state(Initial),
                     c_halt(false), c_pause(false), c_seek(false)
    { }
    
    virtual ~AvkodeThread()
    {
        c_halt = true;
        wait();
    }
    
    bool play() {
        if (m_state == Paused) return resume();
        if (m_state == Opened) load();
        if (m_state != Stopped) return false;
        m_pauseSemaphore.release();
        m_state = Running;
        emit m_parent->setState(Phonon::PlayingState);
        return true;
    }
    bool stop() {
        if (m_state != Running && m_state != Paused) return false;
        c_halt = true;
        if (m_state == Paused) resume();
        wait();
        delete m_decoder; m_decoder = 0;
        m_state = Opened;
        emit m_parent->setState(Phonon::StoppedState);
        return true;
    }

    bool pause() {
        if (m_state != Running) return false;
        c_pause = true;
        m_state = Paused;
        emit m_parent->setState(Phonon::PausedState);
        return true;
    }
    bool resume() {
        if (m_state != Paused) return false;
        m_pauseSemaphore.release();
        m_state = Running;
        emit m_parent->setState(Phonon::PlayingState);
        return true;
    }

    bool open(AvKode::ByteStream *file) {
        if (m_state != Initial) return false;
    /*
        std::string format = AvKode::Magic::detectFile(file);
        kDebug( 604 ) << "AvkodeThread::open() format detected: " << format.c_str() << endl;
        m_decoderHandler.load(format);
        if (!m_decoderHandler.isLoaded()) {
            kDebug( 604 ) << "AvkodeThread::open() decoder failed to load" << endl;
            return false;
        }
    */
        m_file = file;
        m_state = Opened;
        return true;
    }
    bool load() {
        if (m_state != Opened) return false;
        m_parent->setState(Phonon::BufferingState);
        m_decoder = new AvKode::FFMPEGDecoder();
        start(); // start thread
        return true;
    }   
    long position() {
        if (!m_decoder) return 0;
        return m_decoder->position();
    }
    
    long length() {
        if (!m_decoder) return 0;
        return m_decoder->length();
    }
   
    void seek(qint64 pos) {
        m_seekPos = pos;
        c_seek = true;
    }
    bool hasVideo() {
        if (!m_decoder) return false;
        return m_decoder->hasVideo();
    }
    void setPrefinishMark(long pfm) {
        m_prefinishMark = pfm;
    }
    long timeLeft() {
        return m_timeLeft;
    }
    bool seekable() {
        if (!m_decoder) return false;
        return m_decoder->seekable();
    }
protected:
    void run() {
        std::cout << "AvkodeThread::run" << std::endl;
        m_decoder->open(m_file);
        if (!m_decoder->error()) {
            m_parent->setState(Phonon::StoppedState);
            m_state = Stopped;
            emit m_parent->totalTimeChanged(m_decoder->length());
        } else {
            std::cerr << "AvkodeThread::run -  decoder failed to load file" << std::endl;
            return;
        }
        
        if (hasVideo())
            emit m_parent->hasVideoChanged(true);
        if (seekable())
            emit m_parent->seekableChanged(true);
        
        AvKode::AudioFrame aframe;
        AvKode::VideoFrame vframe;
        AvKode::FrameType type;

        if (!m_decoder) {
            std::cerr << "AvkodeThread::run - no decoder!" << std::endl;
            return;
        }

        // Wait for start signal
        m_pauseSemaphore.acquire();
        if (!m_parent->audioOutput() && !m_parent->videoOutput()) return;

        if (m_parent->videoOutput() && m_parent->audioOutput()) {
            m_sync = new AvKode::Synchronizer();
            m_sync->setAudioSink(m_parent->audioOutput());
            m_sync->setVideoSink(m_parent->videoOutput());
        }

        std::cout << "AvkodeThread::run start" << std::endl;
        m_parent->setState(Phonon::PlayingState);
        emit m_parent->tick(0);
        
        dying = false;
        bool prefinishEmitted = false;
        // Thread proper
        while (true) {
            if (c_pause) {
                c_pause = false;
                m_parent->setState(Phonon::PausedState);
                m_pauseSemaphore.acquire();
                m_parent->setState(Phonon::PlayingState);
            }
            if (c_halt) {
                c_halt = false;
                m_parent->setState(Phonon::StoppedState);
                return;
            }

            if (m_decoder->frameType() == AvKode::NoFrameType)
                if (!m_decoder->decodeFrame()) {
                    if (m_decoder->eof() || m_decoder->error())
                        break;
                    else
                        continue;
                }
            type = m_decoder->frameType();
            switch (type) {
                case AvKode::AudioFrameType: {
                    m_decoder->readAudioFrame(&aframe);
                    if (m_sync)
                        m_sync->writeAvFrame(&aframe);
                    else
                    if (m_parent->audioOutput())
                        m_parent->audioOutput()->writeAudioFrame(&aframe);
                    break;
                }
                case AvKode::VideoFrameType: {
                    m_decoder->readVideoFrame(&vframe);
                    if (m_sync)
                        m_sync->writeAvFrame(&vframe);
                    else
                    if (m_parent->videoOutput())
                        m_parent->videoOutput()->writeVideoFrame(&vframe);
                    break;
                }
                default:
                    break;
            }
            long dpos = m_decoder->position();
            if (dpos != -1) {
                long npos = (dpos * 1024.0) / (float)m_decoder->length();
                if ((npos > 0) && (npos < 1024) && m_pos != npos) {
                    emit m_parent->tick(m_decoder->position());
                    m_pos = npos;
                }
                //if (seek_pos == pos) seek_pos = -1;
            }
            m_timeLeft = m_decoder->length() - m_decoder->position(); // Seconds left
            if (m_timeLeft < m_prefinishMark && !m_parent->hasNextSource() && !prefinishEmitted) {
                emit m_parent->prefinishMarkReached(m_timeLeft * 1000);
                prefinishEmitted = true;
            }
            
            if (!dying && m_timeLeft < 1) {
                QMetaObject::invokeMethod(m_parent, "playNextSource");
                //QTimer::singleShot(1000, m_parent, SLOT(playNextSource()));
                dying = true;
            }
                
            if (m_timeLeft < 2) {
                emit m_parent->aboutToFinish();
            }
            
            if (m_decoder->eof() || m_decoder->error()) break;
            if (c_seek) {
                m_decoder->seek(m_seekPos);
                if (m_sync) m_sync->seeked();
                c_seek = false;
            }
        }
        if (!prefinishEmitted && !m_parent->hasNextSource()) {
            emit m_parent->prefinishMarkReached(m_timeLeft);
        }
        emit m_parent->finished();
        m_parent->audioOutput()->close();
        
        if (m_decoder->error())
            m_parent->setState(Phonon::ErrorState);
        else if (!m_parent->hasNextSource())
            m_parent->setState(Phonon::StoppedState);
        
        delete m_sync;
    }
private:
//     AvKode::DecoderPluginHandler m_decoderHandler;
    AvKode::FFMPEGDecoder *m_decoder;
    AvKode::ByteStream *m_file;
    AvKode::Synchronizer *m_sync;
    MediaObject *m_parent;
    long m_pos, m_seekPos, m_prefinishMark, m_timeLeft;

    QSemaphore m_pauseSemaphore;
    enum {Initial, Opened, Stopped, Running, Paused} m_state;

    bool dying;
    
    // control variables:
    volatile bool c_halt;
    volatile bool c_pause;
    volatile bool c_seek;
};


    
    

MediaObject::MediaObject( QObject* parent ) : QObject(parent),
    m_thread(0), m_state(Phonon::BufferingState), m_asink(0), m_vsink(0), m_transitionTime(0)
{
}

MediaObject::~MediaObject()
{
    delete m_thread;
    m_thread = 0;
}

qint64 MediaObject::totalTime() const
{
    if (!m_thread)
        return -1;
    
    return m_thread->length();
}

void MediaObject::setSource(const MediaSource& source)
{
    if (m_thread) {
        m_thread->stop();
        delete m_thread;
    }
    
    m_thread = new AvkodeThread(this);
    m_source = source;
    switch (source.type()) {
        case MediaSource::Url:
            if (source.url().isLocalFile())
                m_file = AvKode::File::open(source.url().toLocalFile().toLocal8Bit());
            else
                m_file = AvKode::KIO::open(source.url()); 
            break;
        case MediaSource::LocalFile:
            m_file = AvKode::File::open(source.fileName().toLocal8Bit());
            break;
        case MediaSource::Empty:
            qWarning() << "Tried to play an Empty MediaSource";
            return;
        case MediaSource::Invalid:
            qWarning() << "Tried to play an invalid MediaSource";
            return;
        default:
            qWarning() << "Unsupported MediaSource type" << source.type();
            return;
    }
    m_thread->open(m_file);
    m_thread->load();
}

qint64 MediaObject::currentTime() const
{
    if (!m_thread)
        return -1;
    
    return m_thread->position();
}
QString MediaObject::errorString() const
{
    return QString();
}
ErrorType MediaObject::errorType() const
{
    return NormalError;
}
bool MediaObject::hasVideo() const
{
    return m_thread && m_thread->hasVideo();
}
bool MediaObject::isSeekable() const
{
    return m_thread && m_thread->seekable();
}
void MediaObject::pause()
{
    if (!m_thread)
        return;
    
    m_thread->pause();
}
void MediaObject::play()
{
    if (!m_thread)
        return;
    
    m_thread->play();
}
qint32 MediaObject::prefinishMark() const
{
    return 0; //###
}
qint64 MediaObject::remainingTime() const
{
    if (!m_thread)
        return -1;
    
    return m_thread->timeLeft();
}
void MediaObject::seek(qint64 milliseconds)
{
    if (!m_thread)
        return;
    
    m_thread->seek(milliseconds);
}
void MediaObject::setNextSource(const MediaSource& source)
{
    m_nextSource = source;
}
void MediaObject::setPrefinishMark(qint32 pfm)
{
    if (!m_thread)
        return;
    
    m_thread->setPrefinishMark(pfm);
}
void MediaObject::setTickInterval(qint32 )
{

}
void MediaObject::setTransitionTime(qint32 transitionTime)
{
    m_transitionTime = transitionTime;
}
MediaSource MediaObject::source() const
{
    return m_source;
}
State MediaObject::state() const
{
    return m_state;
}
void MediaObject::stop()
{
    if (!m_thread)
        return;
    
    m_thread->stop();
}
qint32 MediaObject::tickInterval() const
{
    return 200;
}
qint32 MediaObject::transitionTime() const
{
    return 0;
}

bool MediaObject::addVideoOutput(VideoWidget* output)
{
    if (!m_vsink) {
        m_vsink = new VideoPath(this);
    }
    
    if (!m_vsink->addOutput(output))
        return false;
    return output->open();
}

bool MediaObject::addAudioOutput(AudioOutput* output)
{
    if (!m_asink) {
        m_asink = new AudioPath(this);
    }
    if (!m_asink->addOutput(output))
        return false;
    return output->open();
}

void MediaObject::setState(State current)
{
    if (current == m_state) return;
    
    emit stateChanged(current, m_state);
    m_state = current;
}

void MediaObject::playNextSource()
{
    if (!hasNextSource()) return;
    
    setSource(m_nextSource);
    m_nextSource = MediaSource();
}

bool MediaObject::hasNextSource()
{
    return m_nextSource.type() != Phonon::MediaSource::Empty && m_nextSource.type() != Phonon::MediaSource::Invalid;
}


}//namespace Avkode
}//namespace Phonon

#include "mediaobject.moc"
