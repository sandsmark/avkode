/*  This file is part of the KDE project
    Copyright (C) 2006 Allan Sandfeld Jensen <sandfeld@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301, USA.

*/
#ifndef Phonon_UI_AVKODE_VIDEOWIDGET_H
#define Phonon_UI_AVKODE_VIDEOWIDGET_H

#include <QtCore/QObject>
#include <phonon/experimental/videoframe.h>
#include <phonon/videowidgetinterface.h>
#include "abstractvideooutput.h"

namespace AvKode {
    class VideoSink;
}

class QWidget;

namespace Phonon
{
namespace Avkode
{
    class VideoWidget : public QObject, public Phonon::Avkode::AbstractVideoOutput, public VideoWidgetInterface
    {
    Q_OBJECT
    Q_INTERFACES(Phonon::VideoWidgetInterface)
    
    public:
        VideoWidget( QObject* = 0 );

        // Avkode specific:
        virtual bool open();
        virtual void close();
        virtual bool writeVideoFrame( AvKode::VideoFrame *frame );
        virtual int latency();
        
        // Interface
        virtual Phonon::VideoWidget::AspectRatio aspectRatio() const;
        virtual qreal brightness() const;
        virtual qreal contrast() const;
        virtual qreal hue() const;
        virtual qreal saturation() const;
        virtual Phonon::VideoWidget::ScaleMode scaleMode() const;
        virtual void setAspectRatio(Phonon::VideoWidget::AspectRatio );
        virtual void setBrightness(qreal );
        virtual void setContrast(qreal );
        virtual void setHue(qreal );
        virtual void setSaturation(qreal );
        virtual void setScaleMode(Phonon::VideoWidget::ScaleMode );

    public slots:
        QWidget *widget() { return m_widget; }

    private:
        AvKode::VideoSink *m_sink;
        QWidget *m_widget;
    };
}} //namespace Phonon::Avkode


#endif
