/*  This file is part of the KDE project
    Copyright (C) 2006 Allan Sandfeld Jensen (kde@carewolf.com)

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301, USA.

*/
#ifndef Phonon_AVKODE_VIDEOPATH_H
#define Phonon_AVKODE_VIDEOPATH_H

#include <QObject>
#include <QList>
#include <phonon/phononnamespace.h>

#include <avkode/sink.h>

namespace AvKode
{
    class VideoFrame;
}

namespace Phonon
{
namespace Avkode
{
    class VideoEffect;
    class AbstractVideoOutput;

    class VideoPath : public QObject, public AvKode::VideoSink
    {
    Q_OBJECT
    public:
        VideoPath( QObject* parent );
        ~VideoPath();

        Q_INVOKABLE bool addOutput( QObject* audioOutput );
        Q_INVOKABLE bool removeOutput( QObject* audioOutput );
        Q_INVOKABLE bool insertEffect( QObject* newEffect, QObject* insertBefore = 0 );
        Q_INVOKABLE bool removeEffect( QObject* effect );

        virtual bool open();
        virtual void close();
        virtual bool writeVideoFrame( AvKode::VideoFrame *);
        virtual int latency();
    private:
        AbstractVideoOutput *m_output;
        bool m_open;
    };
}} //namespace Phonon::Avkode

#endif // Phonon_AVKODE_VIDEOPATH_H
