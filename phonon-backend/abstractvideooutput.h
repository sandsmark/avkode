/*  This file is part of the KDE project
    Copyright (C) 2006 Allan Sandfeld Jensen (kde@carewolf.com)

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301, USA.

*/
#ifndef Phonon_AVKODE_ABSTRACTVIDEOOUTPUTBASE_H
#define Phonon_AVKODE_ABSTRACTVIDEOOUTPUTBASE_H

#include <phonon/phononnamespace.h>
#include <avkode/sink.h>

namespace Phonon
{
namespace Avkode
{
    class AbstractVideoOutput : public AvKode::VideoSink
    {
    public:
        virtual ~AbstractVideoOutput() {};

    };
}} //namespace Phonon::Avkode

#endif // Phonon_AKODE_ABSTRACTVIDEOOUTPUTBASE_H
