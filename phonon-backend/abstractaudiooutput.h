/*  This file is part of the KDE project
    Copyright (C) 2006 Allan Sandfeld Jensen (kde@carewolf.com)

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301, USA.

*/
#ifndef Phonon_AVKODE_ABSTRACTAUDIOOUTPUTBASE_H
#define Phonon_AVKODE_ABSTRACTAUDIOOUTPUTBASE_H

#include <QObject>
#include <phonon/phononnamespace.h>
#include <avkode/sink.h>

namespace AvKode {
    class AudioFrame;
}

namespace Phonon
{
namespace Avkode
{
    class AbstractAudioOutput : public QObject, public AvKode::AudioSink
    {
        Q_OBJECT
        public:
            AbstractAudioOutput( QObject* parent );
            virtual ~AbstractAudioOutput();

            virtual bool open() = 0;
            virtual void close() = 0;
//             int setAudioConfiguration(const AudioConfiguration *config);
//             const AudioConfiguration* audioConfiguration() const;
            bool writeAudioFrame( AvKode::AudioFrame *) = 0;
            int latency() = 0;
        private:
    };
}} //namespace Phonon::Avkode

#endif // Phonon_AKODE_ABSTRACTAUDIOOUTPUTBASE_H
