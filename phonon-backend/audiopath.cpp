/*  This file is part of the KDE project
    Copyright (C) 2006 Allan Sandfeld Jensen (kde@carewolf.com)

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301, USA.

*/

#include "audiopath.h"
// #include "audioeffect.h"
#include "abstractaudiooutput.h"

namespace Phonon
{
namespace Avkode
{

AudioPath::AudioPath( QObject* parent )
    : QObject( parent ), m_output(0)
{
}

AudioPath::~AudioPath()
{
}

bool AudioPath::addOutput( QObject* audioOutput )
{
    // we can only handle one output yet
    if (m_output) return false;

    m_output = static_cast<AbstractAudioOutput*>(audioOutput);
    m_output->open();
    return true;
}

bool AudioPath::removeOutput( QObject* audioOutput )
{
    Q_ASSERT( audioOutput );
    if (audioOutput == m_output) {
        m_output->close();
        m_output = 0;
        return true;
    }
    else
        return false;
}

bool AudioPath::insertEffect( QObject* newEffect, QObject* insertBefore )
{
    return false;
}

bool AudioPath::removeEffect( QObject* effect )
{
    return false;
}

bool AudioPath::open()
{
    if (m_output)
        return m_output->open();
    else 
        return true;
}

void AudioPath::close()
{
    if (m_output)
        m_output->close();
}


bool AudioPath::writeAudioFrame(AvKode::AudioFrame *frame)
{
    if (m_output) {
        // convert..
        return m_output->writeAudioFrame(frame);
    } else
        return true;
}

int AudioPath::latency()
{
    if (m_output)
        return m_output->latency();
    else
        return 0;
}
}}

#include "audiopath.moc"
