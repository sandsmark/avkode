/*  This file is part of the KDE project
    Copyright (C) 2006 Allan Sandfeld Jensen (kde@carewolf.com)

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301, USA.

*/

#include <iostream>
#include "audiooutput.h"
#include <QVector>

// #include <config.h>
// #include <config-phonon.h>

#include <avkode/alsa_sink.h>
#include <avkode/converter.h>
#include <avkode/fast_resampler.h>
//
namespace Phonon
{
namespace Avkode
{
AudioOutput::AudioOutput( QObject* parent )
    : AbstractAudioOutput( parent ),
      m_error(false), m_resampler(0), m_converter(0)
{
    m_sink = new AvKode::ALSASink();
    m_resampler = new AvKode::FastResampler(); // ### allow choice of SRC resampler
    m_converter = new AvKode::Converter();
}

AudioOutput::~AudioOutput()
{
}

qreal AudioOutput::volume() const
{
    return m_volume;
}

int AudioOutput::outputDevice() const
{
    return 1;
}

void AudioOutput::setVolume( qreal newVolume )
{
    m_volume = newVolume;
    emit volumeChanged( m_volume );
}

bool AudioOutput::setOutputDevice( int newDevice )
{
    qDebug() << "Asked to set output device:" << newDevice;
    return true;
}

bool AudioOutput::open()
{
    return m_sink->open();
}

void AudioOutput::close()
{
    m_sink->close();
}

void AudioOutput::configureAudio(AvKode::AudioConfiguration config)
{
    m_config = config;
    if (!m_sink) return;
    int state = m_sink->setAudioConfiguration(&config);
    if (state < 0) {
        // configuration failed
        std::cerr << "Configuration failed" << std::endl;
        m_error =  true;
    } else
    {
        AvKode::AudioConfiguration sink_config = *m_sink->audioConfiguration();

        if (config.channels != sink_config.channels) {
            // no channel mixing yet
            m_error = true;
            return;
        }
        m_resampler->setSampleRate(sink_config.sample_rate);
        m_converter->setSampleWidth(sink_config.sample_width);
    }
}

bool AudioOutput::writeAudioFrame(AvKode::AudioFrame *frame)
{
    // Check for new audio configuration
    if (m_config != *frame ) {
        configureAudio(*frame);
    }
    if (m_sink) {
        m_resampler->doFrame(frame, &m_frame);
        m_converter->doFrame(&m_frame);
        if (!m_sink->writeAudioFrame(&m_frame)) {
            emit audioDeviceFailed();
            return false;
        }
    }
    return true;
}

int AudioOutput::latency()
{
    if (m_sink)
        return m_sink->latency(); // add time to do resampling and conversion!
    else
        return 0;
}

}} //namespace Phonon::Avkode

#include "audiooutput.moc"
