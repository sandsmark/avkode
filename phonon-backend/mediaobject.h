/*  This file is part of the KDE project
    Copyright (C) 2006 Allan Sandfeld Jensen (kde@carewolf.com)

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301, USA.

*/
#ifndef Phonon_AVKODE_MEDIAOBJECT_H
#define Phonon_AVKODE_MEDIAOBJECT_H

#include <phonon/phononnamespace.h>
#include <phonon/mediaobjectinterface.h>
#include <phonon/mediasource.h>

namespace AvKode
{
    class ByteStream;
}

namespace Phonon
{
namespace Avkode
{

    class VideoWidget;
    class AudioOutput;
    class AudioPath;
    class VideoPath;

    class MediaObject : public QObject, MediaObjectInterface
    {
        Q_OBJECT
        Q_INTERFACES( Phonon::MediaObjectInterface )
        
        class AvkodeThread;
        friend class AvkodeThread;
        
        public:
            MediaObject( QObject* parent );
            ~MediaObject();
            qint64 totalTime() const;
            virtual QString errorString() const;
            virtual qint64 currentTime() const;
            virtual ErrorType errorType() const;
            virtual bool hasVideo() const;
            virtual bool isSeekable() const;
            virtual void pause();
            virtual void play();
            virtual qint32 prefinishMark() const;
            virtual void seek(qint64 milliseconds);
            virtual void setNextSource(const MediaSource& source);
            virtual void setPrefinishMark(qint32 );
            virtual void setSource(const MediaSource& );
            virtual void setTickInterval(qint32 interval);
            virtual void setTransitionTime(qint32 );
            virtual MediaSource source() const;
            virtual State state() const;
            virtual void stop();
            virtual qint32 tickInterval() const;
            virtual qint32 transitionTime() const;
            virtual qint64 remainingTime() const;
            
            bool addVideoOutput(VideoWidget *output);
            bool addAudioOutput(AudioOutput *output);
            bool hasNextSource();
            VideoPath *videoOutput() const { return m_vsink; }
            AudioPath *audioOutput() const { return m_asink; }
            
        public Q_SLOTS:
            void playNextSource();

        
        Q_SIGNALS:
            void aboutToFinish();
            void stateChanged(Phonon::State old, Phonon::State current);
            void hasVideoChanged(bool);
            void tick(qint64 currentPosition);
            void seekableChanged(bool);
            void bufferStatus(int);
            void metaDataChanged(QMultiMap<QString, QString>);
            void currentSourceChanged(MediaSource);
            void finished();
            void prefinishMarkReached(qint32);
            void totalTimeChanged(qint64);

        private:
            void setState(Phonon::State current);
                
            AvKode::ByteStream *m_file;
            MediaObject::AvkodeThread *m_thread;
            MediaSource m_source;
            MediaSource m_nextSource;
            QString m_error;
            Phonon::State m_state;
            AudioPath *m_asink;
            VideoPath *m_vsink;
            qint32 m_transitionTime;
    };
}} //namespace Phonon::AvKode

#endif // Phonon_AVKODE_MEDIAOBJECT_H
