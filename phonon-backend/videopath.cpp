/*  This file is part of the KDE project
    Copyright (C) 2006 Allan Sandfeld Jensen (kde@carewolf.com)

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301, USA.

*/

#include "videopath.h"
// #include "videoeffect.h"
#include "abstractvideooutput.h"

#include <avkode/avframe.h>
#include <iostream>

namespace Phonon
{
namespace Avkode
{

VideoPath::VideoPath( QObject* parent )
    : QObject( parent ), m_output(0), m_open(false)
{
}

VideoPath::~VideoPath()
{
}

bool VideoPath::addOutput( QObject* videoOutput )
{
    // we can only handle one output yet
    if (m_output) return false;

    m_output = dynamic_cast<AbstractVideoOutput*>(videoOutput);
    if (!m_output) {
        std::cerr << "dynamic cast failed!" << std::endl;
    }
    if (m_open)
        return m_output->open();
    else
        return true;
}

bool VideoPath::removeOutput( QObject* videoOutput )
{
    Q_ASSERT( videoOutput );
    if (dynamic_cast<AbstractVideoOutput*>(videoOutput) == m_output) {
        if (m_open)
            m_output->close();
        m_output = 0;
        return true;
    }
    else
        return false;
}

bool VideoPath::insertEffect( QObject* newEffect, QObject* insertBefore )
{
    return false;
}

bool VideoPath::removeEffect( QObject* effect )
{
    return false;
}

bool VideoPath::writeVideoFrame(AvKode::VideoFrame *frame)
{
    if (m_output) {
        // convert..
        return m_output->writeVideoFrame(frame);
    }
    return true;
}

bool VideoPath::open()
{
    if (m_output)
        m_open = m_output->open();
    else
        m_open = true;
    return m_open;
}

void VideoPath::close()
{
    if (m_output)
        return m_output->close();
    m_open = false;
}

int VideoPath::latency()
{
    if (m_output)
        return m_output->latency();
    else
        return 0;
}

}}

#include "videopath.moc"
