/*  This file is part of the KDE project
    Copyright (C) 2006 Allan Sandfeld Jensen (kde@carewolf.com)

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301, USA.

*/
#ifndef Phonon_AVKODE_AUDIOOUTPUT_H
#define Phonon_AVKODE_AUDIOOUTPUT_H

#include <QFile>

#include <phonon/phononnamespace.h>
#include <avkode/avframe.h>

#include "abstractaudiooutput.h"
#include <phonon/audiooutputinterface.h>

namespace AvKode {
    class AudioSink;
    class AudioFrame;
    class FastResampler;
    class Converter;
    class ALSASink;
}

namespace Phonon
{
namespace Avkode
{
    class AudioOutput : public AbstractAudioOutput, public AudioOutputInterface
    {
        Q_OBJECT
        Q_INTERFACES(Phonon::AudioOutputInterface)
        public:
            AudioOutput( QObject* parent );
            virtual ~AudioOutput();

            // Attributes Getters:
            qreal volume() const;
            int outputDevice() const;

            // Attributes Setters:
            void setVolume( qreal newVolume );
            bool setOutputDevice( int newDevice );

        public:
            bool open();
            void close();

            bool writeAudioFrame( AvKode::AudioFrame *frame );
            int latency();
            
        Q_SIGNALS:
            void volumeChanged( qreal newVolume );
            void audioDeviceFailed();

        private:
            void configureAudio(AvKode::AudioConfiguration config);
            qreal m_volume;
            bool m_error;
            AvKode::ALSASink* m_sink;
            AvKode::FastResampler* m_resampler;
            AvKode::Converter* m_converter;
            AvKode::AudioConfiguration m_config;
            AvKode::AudioFrame m_frame;
    };
}} //namespace Phonon::AvKode

#endif // Phonon_AVKODE_AUDIOOUTPUT_H
