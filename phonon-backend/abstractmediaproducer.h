/*  This file is part of the KDE project
    Copyright (C) 2006 Allan Sandfeld Jensen (kde@carewolf.com)

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301, USA.

*/
#ifndef Phonon_AVKODE_ABSTRACTMEDIAPRODUCER_H
#define Phonon_AVKODE_ABSTRACTMEDIAPRODUCER_H

#include <QObject>
#include <QMultiMap>
#include <QString>

#include <phonon/phononnamespace.h>

#include "audiopath.h"
#include <phonon/abstractmediastream2.h>

class QTimer;

namespace AvKode
{
    class ByteStream;
}
namespace Phonon
{
    class VideoFrame;
    class VideoPath;

namespace Avkode
{
    class KDE_EXPORT AbstractMediaProducer : public QObject, public AbstractMediaStream2
    {
        Q_OBJECT
        Q_INTERFACES( Phonon::AbstractMediaStream2 )
        public:
            AbstractMediaProducer( QObject* parent );
            ~AbstractMediaProducer();
             bool addVideoPath( QObject* videoPath );
             bool addAudioPath( QObject* audioPath );
             void removeVideoPath( QObject* videoPath );
             void removeAudioPath( QObject* audioPath );
             State state() const;
             bool hasVideo() const;
             bool isSeekable() const;
             qint64 currentTime() const;
             qint32 tickInterval() const;

             QStringList availableAudioStreams() const;
             QStringList availableVideoStreams() const;
             QStringList availableSubtitleStreams() const;

             QString selectedAudioStream( const QObject* audioPath ) const;
             QString selectedVideoStream( const QObject* videoPath ) const;
             QString selectedSubtitleStream( const QObject* videoPath ) const;

             void selectAudioStream( const QString& streamName, const QObject* audioPath );
             void selectVideoStream( const QString& streamName, const QObject* videoPath );
             void selectSubtitleStream( const QString& streamName, const QObject* videoPath );

             void setTickInterval( qint32 newTickInterval );
             void play();
             void pause();
             void stop();
             void seek( qint64 time );

             QString errorString() const;
             Phonon::ErrorType errorType() const;

            void setBufferSize( int size );

        Q_SIGNALS:
            void stateChanged( Phonon::State newstate, Phonon::State oldstate );
            void tick( qint64 time );
            void metaDataChanged(const QMultiMap<QString, QString>&);
            void seekableChanged(bool isSeekable); // TODO

        protected:
            void setState( State );
            void setSource( AvKode::ByteStream *file );

        private:
            State m_state;
            AudioPath* m_audioPath;
            VideoPath* m_videoPath;
            qint32 m_tickInterval;

            class AvkodeThread;
            AvkodeThread *m_avkodeThread;
    };

}} //namespace Phonon::Avkode

#endif // Phonon_AVKODE_ABSTRACTMEDIAPRODUCER_H
