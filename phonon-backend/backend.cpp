/*  This file is part of the KDE project
    Copyright (C) 2006 Allan Sandfeld Jensen (kde@carewolf.com)

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301, USA.

*/

#include "backend.h"
#include "mediaobject.h"
// #include "avcapture.h"
// #include "bytestream.h"
#include "audiopath.h"
// #include "audioeffect.h"
#include "audiooutput.h"
// #include "audiodataoutput.h"
// #include "visualization.h"
#include "videopath.h"
// #include "videoeffect.h"
#include "videowidget.h"

// #include "volumefadereffect.h"
// #include "videodataoutput.h"

#include <QSet>
#include <QVariant>
#include <QtCore/QtPlugin>


Q_EXPORT_PLUGIN2(phonon_avkode, Phonon::Avkode::Backend)

namespace Phonon
{
namespace Avkode
{

Backend::Backend( QObject* parent, const QVariantList& )
    : QObject( parent )
{
    setProperty("identifier", QLatin1String("phonon_avkode"));
    setProperty("backendName", QLatin1String("AVKode"));
    setProperty("backendComment", QLatin1String("Lightweight backend for Phonon."));
    setProperty("backendVersion", QLatin1String("0.1"));
    setProperty("backendWebsite", QLatin1String("http://multimedia.kde.org/"));
    
    m_supportedMimeTypes
            << QLatin1String( "audio/vorbis" )
            << QLatin1String( "audio/mpeg" )
            << QLatin1String( "audio/x-flac" )
            << QLatin1String( "audio/x-oggflac" )
            << QLatin1String( "audio/x-speex" )
            << QLatin1String( "audio/x-wav" )
            << QLatin1String( "video/mpeg" )
            << QLatin1String( "video/avi" )
            << QLatin1String( "video/x-msvideo" );
}

Backend::~Backend()
{
}
QStringList Backend::availableMimeTypes() const
{
    return m_supportedMimeTypes;
}
QObject* Backend::createObject(BackendInterface::Class c, QObject* parent, const QList< QVariant >& args)
{
    switch(c) {
    case BackendInterface::MediaObjectClass:
        return new Avkode::MediaObject(parent);
    case BackendInterface::AudioOutputClass:
        return new Avkode::AudioOutput(parent);
    case BackendInterface::VideoWidgetClass:
        return new Avkode::VideoWidget(parent);
    default:
        return 0;
    }
}

bool Backend::startConnectionChange(QSet< QObject* > )
{
    return true;
}
bool Backend::connectNodes(QObject* sourceObject, QObject* sinkObject)
{
    MediaObject *media = qobject_cast<MediaObject*>(sourceObject);
    if (media == 0) {
        qWarning() << "Source is not a media object.";
        return false;
    }
    
    AudioOutput *audioSink = qobject_cast<AudioOutput*>(sinkObject);
    if (audioSink) {
        return media->addAudioOutput(audioSink);
    } else {
        VideoWidget *videoWidget = qobject_cast<VideoWidget*>(sinkObject);
        if (!videoWidget) {
            qWarning() << "Tried to set a sink that's neither a audiooutput nor a videowidget";
            return false;
        }
        return media->addVideoOutput(videoWidget);
    }
}
bool Backend::disconnectNodes(QObject* , QObject* )
{
    return false;
}
bool Backend::endConnectionChange(QSet< QObject* > )
{
    return true;
}
QList< int > Backend::objectDescriptionIndexes(ObjectDescriptionType type) const
{
    return QList<int>();
}
QHash< QByteArray, QVariant > Backend::objectDescriptionProperties(ObjectDescriptionType type, int index) const
{
    return QHash< QByteArray, QVariant > ();
}


}}

#include "backend.moc"
