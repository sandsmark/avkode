/*  This file is part of the KDE project
    Copyright (C) 2006 Allan Sandfeld Jensen <sandfeld@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301, USA.

*/

#include "videowidget.h"
#include <kdebug.h>

#include <avkode/xv_sink.h>
#include <QtGui/QVBoxLayout>

namespace Phonon
{
namespace Avkode
{

VideoWidget::VideoWidget(QObject * parent) : QObject( parent )
{
    AvKode::XVideoSink *sink = new AvKode::XVideoSink(qobject_cast<QWidget*>(parent));
    m_sink = sink;
    m_widget = sink;
    m_widget->setMinimumSize( 320, 240 );
}

bool VideoWidget::open() {
    if (m_sink)
        return m_sink->open();
    else
        return true;
}

void VideoWidget::close() {
    if (m_sink)
        m_sink->close();
    delete m_sink;
}

bool VideoWidget::writeVideoFrame( AvKode::VideoFrame *frame )
{
    if (m_sink)
        return m_sink->writeVideoFrame(frame);
    return true;
}

int VideoWidget::latency()
{
    if (m_sink)
        return m_sink->latency();
    else
        return 0;
}

Phonon::VideoWidget::AspectRatio VideoWidget::aspectRatio() const
{
    return Phonon::VideoWidget::AspectRatioAuto;
}

qreal VideoWidget::brightness() const
{
    return 0;
}

qreal VideoWidget::contrast() const
{
    return 0;
}

qreal VideoWidget::hue() const
{
    return 0;
}

qreal VideoWidget::saturation() const
{
    return 0;
}

Phonon::VideoWidget::ScaleMode VideoWidget::scaleMode() const
{
    return Phonon::VideoWidget::FitInView;
}

void VideoWidget::setAspectRatio(Phonon::VideoWidget::AspectRatio )
{
    
}

void VideoWidget::setBrightness(qreal )
{
    
}

void VideoWidget::setContrast(qreal )
{

}

void VideoWidget::setHue(qreal )
{

}

void VideoWidget::setSaturation(qreal )
{

}

void VideoWidget::setScaleMode(Phonon::VideoWidget::ScaleMode )
{

}

}} //namespace Phonon::Avkode

#include "videowidget.moc"
