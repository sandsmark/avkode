/*  This file is part of the KDE project
    Copyright (C) 2006 Allan Sandfeld Jensen (kde@carewolf.com)

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301, USA.

*/

#ifndef Phonon_AVKODE_BACKEND_H
#define Phonon_AVKODE_BACKEND_H

#include <QtCore/QObject>
#include <QtCore/QList>
#include <QtCore/QPointer>
#include <QtCore/QStringList>
#include <phonon/phononnamespace.h>
#include <phonon/objectdescription.h>
#include <phonon/backendinterface.h>

class KUrl;

namespace Phonon
{
namespace Avkode
{
    class AudioOutput;

    class Backend : public QObject, public BackendInterface
    {
        Q_OBJECT
        Q_INTERFACES(Phonon::BackendInterface)
        public:
            Backend( QObject* parent = 0, const QVariantList &arguments = QVariantList());
            virtual ~Backend();

            virtual QStringList availableMimeTypes() const;
            virtual bool connectNodes(QObject* , QObject* );
            virtual QObject* createObject(Class c, QObject* parent, const QList< QVariant >& args = QList<QVariant>());
            virtual bool disconnectNodes(QObject* , QObject* );
            virtual bool endConnectionChange(QSet< QObject* > );
            virtual QList< int > objectDescriptionIndexes(ObjectDescriptionType type) const;
            virtual QHash< QByteArray, QVariant > objectDescriptionProperties(ObjectDescriptionType type, int index) const;
            virtual bool startConnectionChange(QSet< QObject* > );

        private:
            QStringList m_supportedMimeTypes;
            QList<QPointer<AudioOutput> > m_audioOutputs;
    };
}} // namespace Phonon::AvKode

#endif // Phonon_AVKODE_BACKEND_H
