/*  This file is part of the KDE project
    Copyright (C) 2006 Allan Sandfeld Jensen (kde@carewolf.com)

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301, USA.

*/

#include <assert.h>

#include <cmath>

#include <QTimer>
#include <QVector>
#include <QFile>
#include <QByteArray>
#include <QStringList>
#include <QThread>
#include <QSemaphore>

#include <kdebug.h>

#include "videopath.h"
#include "audiopath.h"
#include "abstractmediaproducer.h"

#include <avkode/avframe.h>
#include <avkode/synchronizer.h>
#include <avkode/ffmpeg_decoder.h>
// #include <avkode/magic.h>

namespace Phonon
{
namespace Avkode
{


AbstractMediaProducer::AbstractMediaProducer( QObject* parent )
    : QObject( parent )
    , m_state( Phonon::LoadingState )
{
    //kDebug( 604 ) << k_funcinfo << endl;
    m_avkodeThread = new AvkodeThread;
}

AbstractMediaProducer::~AbstractMediaProducer()
{
    //kDebug( 604 ) << k_funcinfo << endl;
}

void AbstractMediaProducer::setBufferSize( int size )
{
    Q_UNUSED( size );
    //m_bufferSize = size;
}

bool AbstractMediaProducer::addVideoPath( QObject* videoPath )
{
    kDebug( 604 ) << k_funcinfo << endl;
    Q_ASSERT( videoPath );
    m_videoPath = static_cast<VideoPath*>(videoPath);
    m_avkodeThread->setVideoPath(m_videoPath);
    return false;
}

bool AbstractMediaProducer::addAudioPath( QObject* audioPath )
{
    kDebug( 604 ) << k_funcinfo << endl;
    Q_ASSERT( audioPath );
    m_audioPath = static_cast<AudioPath*>(audioPath);
    m_avkodeThread->setAudioPath(m_audioPath);
    return true;
}

void AbstractMediaProducer::removeVideoPath( QObject* videoPath )
{
    Q_UNUSED( videoPath );
}

void AbstractMediaProducer::removeAudioPath( QObject* audioPath )
{
    Q_UNUSED( audioPath );
    // ### stop
    m_audioPath = 0;
}

State AbstractMediaProducer::state() const
{
//     kDebug( 604 ) << k_funcinfo << endl;
    return m_state;
}

bool AbstractMediaProducer::hasVideo() const
{
    //kDebug( 604 ) << k_funcinfo << endl;
    return false;
}

bool AbstractMediaProducer::isSeekable() const
{
    //kDebug( 604 ) << k_funcinfo << endl;
    // ### all the paths need to be seekable
    return true;
}

qint64 AbstractMediaProducer::currentTime() const
{
    //kDebug( 604 ) << k_funcinfo << endl;
    switch( state() )
    {
        case Phonon::PausedState:
        case Phonon::BufferingState:
//             return m_startTime.msecsTo( m_pauseTime );
        case Phonon::PlayingState:
//             return m_startTime.elapsed();
        case Phonon::StoppedState:
        case Phonon::LoadingState:
            return 0;
        case Phonon::ErrorState:
            break;
    }
    return -1;
}

qint32 AbstractMediaProducer::tickInterval() const
{
    //kDebug( 604 ) << k_funcinfo << endl;
    return m_tickInterval;
}

void AbstractMediaProducer::setTickInterval( qint32 newTickInterval )
{
    //kDebug( 604 ) << k_funcinfo << endl;
    m_tickInterval = newTickInterval;
    /*
    if( m_tickInterval <= 0 )
        m_tickTimer->setInterval( 50 );
    else
        m_tickTimer->setInterval( newTickInterval );
    */
}

QStringList AbstractMediaProducer::availableAudioStreams() const
{
    QStringList ret;
    ret << QLatin1String( "default" );
    return ret;
}

QStringList AbstractMediaProducer::availableVideoStreams() const
{
    QStringList ret;
    return ret;
}

QStringList AbstractMediaProducer::availableSubtitleStreams() const
{
    QStringList ret;
    return ret;
}

QString AbstractMediaProducer::selectedAudioStream( const QObject* audioPath ) const
{
    Q_UNUSED( audioPath );
    return QString();
}

QString AbstractMediaProducer::selectedVideoStream( const QObject* videoPath ) const
{
    Q_UNUSED( videoPath );
    return QString();
}

QString AbstractMediaProducer::selectedSubtitleStream( const QObject* videoPath ) const
{
    Q_UNUSED( videoPath );
    return QString();
}

void AbstractMediaProducer::selectAudioStream( const QString& streamName, const QObject* audioPath )
{
    Q_UNUSED( streamName );
    Q_UNUSED( audioPath );
    // ### Okay??
}

void AbstractMediaProducer::selectVideoStream( const QString& streamName, const QObject* videoPath )
{
    Q_UNUSED( streamName );
    Q_UNUSED( videoPath );
}

void AbstractMediaProducer::selectSubtitleStream( const QString& streamName, const QObject* videoPath )
{
    Q_UNUSED( streamName );
    Q_UNUSED( videoPath );
}

void AbstractMediaProducer::play()
{
    kDebug( 604 ) << k_funcinfo << endl;
    setState( Phonon::PlayingState );
    bool success = m_avkodeThread->play();
    if (!success)
        kDebug( 604 ) << "AbstractMediaProducer::play() failed" << endl;
}

void AbstractMediaProducer::pause()
{
    kDebug( 604 ) << k_funcinfo << endl;
    setState( Phonon::PausedState );
    m_avkodeThread->pause();
}

void AbstractMediaProducer::stop()
{
    kDebug( 604 ) << k_funcinfo << endl;
    setState( Phonon::StoppedState );
    bool success = m_avkodeThread->stop();
    if (!success)
        kDebug( 604 ) << "AbstractMediaProducer::stop() failed" << endl;
}

void AbstractMediaProducer::seek( qint64 time )
{
    //kDebug( 604 ) << k_funcinfo << endl;
    // ### seek!
}

QString AbstractMediaProducer::errorString() const
{
    // TODO
    return QString();
}

Phonon::ErrorType AbstractMediaProducer::errorType() const
{
    // TODO
    return Phonon::NoError;
}

void AbstractMediaProducer::setState( State newstate )
{
    if( newstate == m_state )
        return;
    State oldstate = m_state;
    m_state = newstate;
    emit stateChanged( newstate, oldstate );
}

void AbstractMediaProducer::setSource(AvKode::ByteStream *file)
{
    kDebug( 604 ) << k_funcinfo << endl;
    m_avkodeThread->open(file);
    m_avkodeThread->load();
}

}}
#include "abstractmediaproducer.moc"
